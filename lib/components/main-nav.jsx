/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Main logged In Nav",

  render() {
    return (
      <ul className="navigation">
        <li className="navigation__item">
          <a
            href="https://fuzzy.ai/"
            className="navigation__link navigation__link__active"
          >
            Home
          </a>
        </li>
        <li className="navigation__item">
          <a href="https://fuzzy.ai/how-it-works" className="navigation__link">
            How it works
          </a>
        </li>
        <li className="navigation__item">
          <Link to="/" className="navigation__link">
            Docs
          </Link>
        </li>
        <li className="navigation__item">
          <a href="https://fuzzy.ai/pricing" className="navigation__link">
            Pricing
          </a>
        </li>
        <li className="navigation__item">
          <a href="https://fuzzy.ai/about" className="navigation__link">
            About
          </a>
        </li>
        <li className="navigation__item">
          <a href="http://blog.fuzzy.ai/" className="navigation__link">
            Blog
          </a>
        </li>
      </ul>
    );
  }
});
