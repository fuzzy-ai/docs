/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Btn",

  render() {
    return (
      <div>
        <Link to={this.props.link} className={this.props.class}>
          {this.props.text}
        </Link>
      </div>
    );
  }
});
