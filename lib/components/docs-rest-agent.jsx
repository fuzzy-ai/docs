/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Docs",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1>Agent</h1>
        <p>
          For more advanced applications, you can manage your agents through the
          API. All of these endpoints use the Agent datatype.
        </p>
        <p>This is the JSON representation of a fuzzy.ai fuzzy agent.</p>
        <table cellPadding="0" cellSpacing="0">
          <tbody>
            <tr>
              <th className="upper fwb">{`\
Property\
`}</th>
              <th className="upper fwb">{`\
Purpose\
`}</th>
              <th className="upper fwb">{`\
Example\
`}</th>
            </tr>
            <tr>
              <td data-title="Property">
                <p>id</p>
              </td>
              <td data-title="Purpose">
                <p>Unique ID of the agent; an opaque string</p>
              </td>
              <td data-title="Example">
                <pre> "a38t196fd" </pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>name</p>
              </td>
              <td data-title="Purpose">
                <p>The name of the agent</p>
              </td>
              <td data-title="Example">
                <pre> "Social Relevance" </pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>inputs</p>
              </td>
              <td data-title="Purpose">
                <p>
                  An object mapping input names to input definitions. Each
                  definition is an object mapping fuzzy set names to set
                  definitions. Each fuzzy set definition is an array of numbers,
                  representing a slope (2 numbers), triangle (3 numbers), or a
                  trapezoid (4 numbers).{" "}
                </p>
              </td>
              <td data-title="Example">
                <pre>{`{"numberOfLikes": \n \
{ \n \
"veryLow": [0, 10], \n \
"low": [0, 5, 15, 20], \n \
"medium": [10, 15, 20, 25], \n \
"high": [20, 30, 70, 80], \n \
"veryHigh": [60, 100] \n \
}, \n \
"numberOfShares": \n \
{ \n \
"veryLow": [0, 5], \n \
"low": [0, 5, 10], \n \
"medium": [5, 10, 20, 25], \n \
"high": [20, 25, 50, 55], \n \
"veryHigh": [50, 75] \n \
}, \n \
"age": \n \
{ \n \
"brandNew": [0, 15], \n \
"new": [0, 15, 50, 60], \n \
"recent": [45, 60, 120, 135] \n \
"old": [120, 240] \n \
} \n \
}`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>outputs</p>
              </td>
              <td data-title="Purpose">
                <p>
                  An object mapping output names to output definitions. Each
                  output definition is an object mapping fuzzy set names to set
                  definitions. Each fuzzy set definition is an array of numbers,
                  representing a slope (2 numbers), triangle (3 numbers), or a
                  trapezoid (4 numbers).
                </p>
              </td>
              <td data-title="Example">
                <pre>{`\
{ \n \
"relevance": { \n \
"veryLow": [0, 20],\n \
"low": [5, 25, 45],\n \
"medium": [30, 50, 70],\n \
"high": [55, 75, 95],\n \
"veryHigh": [80, 100]\n \
} \n \
}`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>rules</p>
              </td>
              <td data-title="Purpose">
                <p>
                  An array of strings, one for each rule in the agent. The rule
                  language is a subset of{" "}
                  <a href="http://en.wikipedia.org/wiki/Fuzzy_Control_Language">
                    Fuzzy Control Language
                  </a>, mapping simple antecedents (possibly joined with AND or
                  OR) with simple consequents.{" "}
                </p>
              </td>
              <td data-title="Example">
                <pre>{`\
[
  \"IF age IS new THEN relevance IS high\",
  \"IF numberOfLikes IS medium THEN relevance IS medium\"
]\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>createdAt</p>
              </td>
              <td data-title="Purpose">
                <p>ISO 8601 timestamp for the create date of the Agent.</p>
              </td>
              <td data-title="Example">
                <pre> "2015-02-20T16:32:56.358Z" </pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>updatedAt</p>
              </td>
              <td data-title="Purpose">
                <p>
                  ISO 8601 timestamp for the last update date of the Agent. May
                  be identical to the createdAt date.
                </p>
              </td>
              <td data-title="Example">
                <pre>"2015-02-20T16:32:56.358Z"</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>latestVersion</p>
              </td>
              <td data-title="Purpose">
                <p>The ID of the latest version of the agent.</p>
              </td>
              <td data-title="Example">
                <pre>"abbaNZ191"</pre>
              </td>
            </tr>
          </tbody>
        </table>
        <a className="docs" id="listagents" name="listagents" />
        <h3>GET https://api.fuzzy.ai/agent</h3>
        <p>
          This method returns a list of all the fuzzy agents associated with
          your account.
        </p>
        <h3>Request</h3>
        <p>The request body is empty. This method requires authentication.</p>
        <h3>Response</h3>
        <p>
          The response body is a UTF-8-encoded JSON array of abbreviated agent
          objects. Each agent object includes an <em>id</em> and a <em>name</em>{" "}
          property. For example
        </p>
        <pre>
          {`\
[\
`}
          {` \n \
{"id": "aZ1avN6F", "name": "Social Relevance"}, \n \
{"id": "thx138ddd", "name", "Dynamic Pricing"}, \n \
{"id": "0xDeaDBeef", "name": "Fraud Detection"} \n\
`}
          {`\
]\
`}
        </pre>
        <h3>POST https://api.fuzzy.ai/agent</h3>
        <p>This method is used to create a new agent.</p>
        <h3>Request</h3>
        <p>{`The request body is a UTF-8-encoded JSON representation of
an Agent. The ID is not required.`}</p>
        <h3>Response</h3>
        <p>
          The response body is a UTF-8-encoded JSON representation of the Agent,
          with the ID added. The Location header includes the full endpoint for
          the new Agent.
        </p>
        <p>
          <a className="docs" id="getagent" name="getagent" />
        </p>
        <h3>
          GET https://api.fuzzy.ai/agent/<em>agent id</em>
        </h3>
        <p>This endpoint retrieves the current definition of the Agent.</p>
        <h3>Request</h3>
        <p>The request body is empty. This method requires authentication.</p>
        <h3>Response</h3>
        <p>
          The response body is an UTF-8-encoded JSON representation of the Agent
          (see above for details).
        </p>
        <p>
          <a className="docs" id="updateagent" name="updateagent" />
        </p>
        <h3>
          PUT https://api.fuzzy.ai/agent/<em>agent id</em>
        </h3>
        <p>
          This method updates the definition of the Agent with the given ID.
        </p>
        <h3>Request</h3>
        <p>
          The request body is an UTF-8-encoded JSON representation of the Agent.
          All properties except <em>name</em>, <em>inputs</em>, <em>outputs</em>{" "}
          and <em>rules</em> are ignored. This method requires authentication.
        </p>
        <h3>Response</h3>
        <p>
          The response body is an UTF-8-encoded JSON representation of the
          Agent. The <em>updatedAt</em> timestamp will be updated, as will be
          the <em>latestVersion </em> property.
        </p>
        <p>
          <a className="docs" id="deleteagent" name="deleteagent" />
        </p>
        <h3>
          DELETE https://api.fuzzy.ai/agent/<em>agent id</em>
        </h3>
        <p>
          This method deletes an Agent, including its version history (see
          below).
        </p>
        <h3>Request</h3>
        <p>The request body should be empty.</p>
        <h3>Response</h3>
        <p>
          The response is an UTF-8-encoded JSON object representing the deletion
          of the objection. It includes a single property <em>message</em> which
          will have the value "OK".
        </p>
      </div>
    );
  }
});
