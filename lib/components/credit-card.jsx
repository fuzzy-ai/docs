/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");

module.exports = React.createClass({
  displayName: "CreditCard",

  handleSubmit(e) {
    e.preventDefault();
    const { submitHandler } = this.props;
    return submitHandler(e.target);
  },

  // coffeelint: disable=max_line_length
  render() {
    const { inProgress } = this.props;
    return (
      <div className="toggled-credit-card ">
        <form method="post" onSubmit={this.handleSubmit} className="user-forms">
          <div className="input-wrap">
            <label htmlFor="name" className="upper">
              Name
            </label>
            <input type="text" id="name" name="name" data-stripe="name" />
          </div>
          <div className="input-wrap">
            <label htmlFor="name" className="upper">
              Zip / Postal
            </label>
            <input
              type="text"
              id="address_zip"
              name="address_zip"
              data-stripe="address_zip"
            />
          </div>
          <div className="input-wrap">
            <label htmlFor="number" className="upper">
              Credit Card Number
            </label>
            <input
              type="text"
              size="20"
              id="number"
              name="number"
              data-stripe="number"
            />
          </div>
          <div className="input-wrap input-wrap-1of3">
            <label htmlFor="cvc" className="upper">
              CVC
            </label>
            <input type="text" size="4" id="cvc" name="cvc" data-stripe="cvc" />
          </div>
          <div className="input-wrap input-wrap-2of3">
            <label htmlFor="exp-month" className="upper">
              Expiration
            </label>
            <input
              type="text"
              size="2"
              className="border-r-none small-input-1of3"
              id="exp-month"
              name="exp-month"
              data-stripe="exp-month"
              placeholder="MM"
            />
            <input
              type="text"
              size="4"
              className="border-l-none small-input-2of3"
              id="exp-year"
              name="exp-year"
              data-stripe="exp-year"
              placeholder="YYYY"
            />
          </div>
          {inProgress ? (
            <button
              className="btn btn__regular"
              disabled="disabled"
              type="submit"
            >
              Working...
            </button>
          ) : (
            <button className="btn btn__regular" type="submit">
              Save
            </button>
          )}
        </form>
      </div>
    );
  }
});
