/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "fuzzy footer nav",

  render() {
    return (
      <ul className="footer-nav">
        <li className="footer-nav__item fwb">
          <a href="https://fuzzy.ai/about">About Fuzzy.ai</a>
        </li>
        <li className="footer-nav__item fwb">
          <a href="https://fuzzy.ai/tos">Terms of Service</a>
        </li>
        <li className="footer-nav__item fwb">
          <a href="https://fuzzy.ai/privacy">Privacy Policy</a>
        </li>
      </ul>
    );
  }
});
