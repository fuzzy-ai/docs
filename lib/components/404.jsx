/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");

module.exports = React.createClass({
  displayName: "FourOhFour",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div className="row">
        <div className="page">
          <h1 className="fwb four-oh-four">404</h1>
          <p>We can’t seem to find the page you are looking for</p>
        </div>
      </div>
    );
  }
});
