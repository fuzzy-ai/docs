/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");
const { connect } = require("react-redux");

const MainLoggedInNav = require("./main-logged-in-nav");
const MainNav = require("./main-nav");
const UserNav = require("./user-nav");

const Navbar = React.createClass({
  displayName: "Navbar",

  logoutClick(e) {
    let dispatch;
    e.preventDefault();
    return ({ dispatch } = this.props);
  },

  getInitialState() {
    return {
      navOpen: false,
      overlay: false,
      burgerMenu: false,
      settings: false
    };
  },

  toggleNav() {
    this.setState({ navOpen: false });
    this.setState({ overlay: true });
    this.setState({ burgerMenu: true });

    this.setState({ navOpen: !this.state.navOpen });
    this.setState({ overlay: !this.state.overlay });
    return this.setState({ burgerMenu: !this.state.burgerMenu });
  },

  toggleSettings() {
    this.setState({ settings: !this.state.settings });
    return this.setState({ settingsOverlay: !this.state.settingsOverlay });
  },

  componentDidMount() {
    const masthead = document.querySelector(".masthead");
    const masthead_height = getComputedStyle(masthead).height.split("px")[0];

    const top = masthead != null ? masthead.offsetTop : undefined;

    const listener = function() {
      const y = window.pageYOffset;
      if (y >= 110) {
        masthead.classList.add("masthead_lined");
      } else {
        masthead.classList.remove("masthead_lined");
      }
    };

    return window.addEventListener("scroll", listener, false);
  },

  // coffeelint: disable=max_line_length
  render() {
    let burgerMenuClass,
      handleNavToggle,
      handleSettings,
      MobiNavClass,
      overlayClass,
      settingsClass,
      settingsOverlayClass,
      settingsTriggerClass;
    const { authenticated, account } =
      (this.props != null ? this.props.user : undefined) != null;

    const {
      navOpen,
      overlay,
      burgerMenu,
      settings,
      settingsOverlay
    } = this.state;

    if (navOpen) {
      MobiNavClass = "mobi_nav mobi_nav--is_active";
      overlayClass = "overlay--is_active";
      burgerMenuClass = "burger-lines actived-menu";
      handleNavToggle = this.toggleNav;
    } else {
      MobiNavClass = "mobi_nav  mobi_nav--is_closed";
      overlayClass = "overlay";
      burgerMenuClass = "burger-lines";
      handleNavToggle = this.toggleNav;
    }

    if (settings) {
      settingsClass = "settings__user-controls open_settings";
      settingsTriggerClass = "setting__trigger settings__trigger--closed";
      settingsOverlayClass = "setting__overlay ";
      handleSettings = this.toggleSettings;
    } else {
      settingsClass = "settings__user-controls";
      settingsTriggerClass = "setting__trigger";
      settingsOverlayClass = "setting__overlay setting__overlay--closed";

      handleSettings = this.toggleSettings;
    }

    return (
      <div className="masthead">
        <div className="logo-wrap flex">
          <Link to="https://fuzzy.ai/" className="logo">
            <img src="/assets/i/fuzzy-ai-logo.svg" id="logo_svg" />
          </Link>
        </div>
        <div className={MobiNavClass} onClick={handleNavToggle}>
          {authenticated ? <MainLoggedInNav /> : <MainNav />}
          {authenticated ? (
            <div className="settings__user-controls ">
              <span className="user-detail__email line">{account.email}</span>
              <span className="user-detail__plan line">
                Current plan: <em>{account.plan}</em>
              </span>
              <Link to="https://fuzzy.ai/settings">settings</Link>
              <Link
                to="/logout"
                onClick={this.logoutClick}
                className=" user-action-logout"
              >
                Logout
              </Link>
            </div>
          ) : (
            <UserNav />
          )}
        </div>
        <div className={overlayClass} onClick={handleNavToggle} />
        <div className={settingsOverlayClass} onClick={handleSettings} />
        <div className={burgerMenuClass} onClick={handleNavToggle}>
          <span />
        </div>
        <div className="nav-mixed menu">
          <div className="main-nav">
            <nav
              id="multi-level-nav"
              className="multi-level-nav"
              role="navigation"
            >
              {authenticated ? <MainLoggedInNav /> : <MainNav />}
            </nav>
          </div>
        </div>
        <div className="user-nav">
          <nav id="login_menu" className="login_menu" role="navigation">
            <div className="login_menu_inner">
              {authenticated ? (
                <div className="user" onClick={handleSettings}>
                  <div className="user__logged-in">
                    <div className="user-detail">
                      <span className="user-detail__email line">
                        {account.email}
                      </span>
                      <span className="user-detail__plan line">
                        Current plan: <em>{account.plan}</em>
                      </span>
                    </div>
                  </div>
                  <div className="settings">
                    <div
                      className={settingsTriggerClass}
                      onClick={handleSettings}
                    >
                      <span className="settings__triangle" />
                      <span className="settings__triangle" />
                      <span className="settings__triangle" />
                    </div>
                  </div>
                  <div className={settingsClass} onClick={handleSettings}>
                    <span className="user-detail__email line">
                      {account.email}
                    </span>
                    <span className="user-detail__plan line">
                      Current plan: <em>{account.plan}</em>
                    </span>
                    <Link to="https://fuzzy.ai/settings">settings</Link>
                    <Link
                      to="/logout"
                      onClick={this.logoutClick}
                      className=" user-action-logout"
                    >
                      Logout
                    </Link>
                  </div>
                </div>
              ) : (
                <UserNav />
              )}
            </div>
          </nav>
        </div>
      </div>
    );
  }
});

const mapStateToProps = state => ({
  user: state != null ? state.user : undefined
});

module.exports = connect(mapStateToProps)(Navbar);
