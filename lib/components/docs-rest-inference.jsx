/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Docs",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1 className="h2 fwb">Inference</h1>
        <p>
          The most important functionality of the fuzzy.ai API is doing fuzzy
          inference with a single agent. In fuzzy inference, you provide input
          values to the fuzzy agent, and it makes decisions based on those
          inputs to provide you with outputs.
        </p>
        <p>Most applications will only need this endpoint.</p>
        <pre>
          POST https://api.fuzzy.ai/agent/<em>
            <em>agent id</em>
          </em>
        </pre>
        <p>
          To do inference, do an HTTP POST request to the API URL for your
          agent. This requires the  <em>agent id</em>, which is available on the
          agent page in the fuzzy.ai Web UI.
        </p>
        <h2>Request</h2>
        <p>
          The body of the request must be UTF-8-encoded JSON. It is an object
          whose properties are the names of the inputs for your agent, and whose
          values are integers or floating-point numbers that are the values of
          those inputs. For example:
        </p>
        <pre>{`{ \n \
"numberOfLikes": 18, \n \
"numberOfShares": 4, \n \
"age": 33 \n \
}`}</pre>
        <p>
          Be careful not to include strings or other JSON types as the values of
          the inputs. fuzzy.ai only works with numbers!
        </p>
        <h3>Response</h3>
        <p>
          The body of the response is an object whose properties are the names
          of the outputs of your agent, and whose values are the (inferred)
          values of those outputs. For example:
        </p>
        <pre>{`{ \n \
"relevance": 50.5 \n \
}`}</pre>
        <p>
          Note that the agent will return an object even if there is only one
          output.
        </p>
        <p>
          The response will include a custom HTTP header,{" "}
          <em>X-Evaluation-ID</em>. You can use this ID for the evaluation
          portion of the API, described below.
        </p>
        <h3>Batch inference</h3>
        <p>
          Alternately, you can post a batch of inputs. This is a JSON array,
          with each element an object. For example:
        </p>
        <pre>
          {`\
[\
`}
          {` \n \
{ \n \
"numberOfLikes": 4, \n \
"numberOfShares": 8, \n \
"age": 15 \n \
}, \n \
{ \n \
"numberOfLikes": 16, \n \
"numberOfShares": 23, \n \
"age": 42 \n \
}, \n\
`}
          {`\
]\
`}
        </pre>
        <p>{`\
For your API limit, each object counts as an evaluation!
It is recommended to have at most 100 objects per batch.
There is a hard limit of 4096 objects per batch.\
`}</p>
        <p>
          If you posted a batch of inputs, you will get a batch of outputs back:
        </p>
        <pre>
          {`\
[\
`}
          {`\n \
{ \n \
"relevance": 4.8 \n \
},\n \
{ \n \
"relevance": 15.16\n \
} \n\
`}
          {`\
]\
`}
        </pre>
        <p>
          The batch results are in order; the inputs at position 3 in the
          request array will generate the output in position 3 in the response
          array.{" "}
        </p>
        <p>
          If you do batch evaluations, the X-Evaluation-ID will not be provided.
          If you need evaluation IDs for learning, you need to set the meta
          flag, as described below.{" "}
        </p>
        <h3>Metadata</h3>
        <p>
          It is possible to get the metadata for an inference in the results of
          the inference. To do this, add a <code>meta</code> parameter to the
          inference endpoint.
        </p>
        <pre>
          POST https://api.fuzzy.ai/agent/<em>
            <em>agent id</em>
          </em>?meta=true
        </pre>
        <p>
          If the <code>meta</code> parameter is any truthy-sounding value, like{" "}
          <code>true</code>, <code>on</code>, <code>yes</code>, or{" "}
          <code>1</code>, then the metadata for the evaluation will be returned
          in the outputs as a property <code>meta</code>.
        </p>
        <p>
          If the parameter is any other value, then the metadata will be
          returned in the outputs as a property with that value as the name. So{" "}
          <code>meta=audit</code> will result in an <code>audit</code> property
          in the outputs. This would be useful on the unlikely chance that you
          already have a <code>meta</code> property in your outputs.
        </p>
        <p>
          Each <code>meta</code> property in the outputs will be an object with
          the same properties as an{" "}
          <Link to="/docs/rest/evaluation">evaluation</Link>.{" "}
        </p>
        <p>
          If you do batch inference, and you include the <code>meta</code> flag,
          a <code>meta</code> property is added to each output.{" "}
        </p>
      </div>
    );
  }
});
