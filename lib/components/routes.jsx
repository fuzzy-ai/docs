/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { render } = require("react-dom");
const {
  Router,
  Route,
  IndexRoute,
  Redirect,
  browserHistory
} = require("react-router");

const App = require("./app");
const NotFound = require("./404");
const DocsIndex = require("./docs-getting-started");
const DocsGettingStarted = require("./docs-getting-started");
const DocsSDK = require("./docs-sdk");
const DocsSDKIndex = require("./docs-sdk-index");
const DocsSDKNodejs = require("./docs-sdk-nodejs");
const DocsSDKPHP = require("./docs-sdk-php");
const DocsSDKPython = require("./docs-sdk-python");
const DocsSDKRuby = require("./docs-sdk-ruby");
const DocsREST = require("./docs-rest");
const DocsRESTIndex = require("./docs-rest-index");
const DocsRESTAboutREST = require("./docs-rest-about-rest");
const DocsRESTData = require("./docs-rest-data");
const DocsRESTAuthentication = require("./docs-rest-authentication");
const DocsRESTInference = require("./docs-rest-inference");
const DocsRESTAgent = require("./docs-rest-agent");
const DocsRESTAgentVersion = require("./docs-rest-agent-version");
const DocsRESTEvaluation = require("./docs-rest-evaluation");
const DocsRESTAPIVersion = require("./docs-rest-api-version");

// coffeelint: disable=max_line_length
module.exports = store => (
  <Route path="/" component={App}>
    <IndexRoute component={DocsIndex} />
    <Route path="getting-started" component={DocsGettingStarted} />
    <Route path="sdk" component={DocsSDK}>
      <IndexRoute component={DocsSDKIndex} />
      <Route path="nodejs" component={DocsSDKNodejs} />
      <Route path="php" component={DocsSDKPHP} />
      <Route path="python" component={DocsSDKPython} />
      <Route path="ruby" component={DocsSDKRuby} />
    </Route>
    <Route path="rest" component={DocsREST}>
      <IndexRoute component={DocsRESTIndex} />
      <Route path="about-rest" component={DocsRESTAboutREST} />
      <Route path="data" component={DocsRESTData} />
      <Route path="authentication" component={DocsRESTAuthentication} />
      <Route path="inference" component={DocsRESTInference} />
      <Route path="agent" component={DocsRESTAgent} />
      <Route path="agent-version" component={DocsRESTAgentVersion} />
      <Route path="evaluation" component={DocsRESTEvaluation} />
      <Route path="api-version" component={DocsRESTAPIVersion} />
    </Route>
    <Route path="*" component={NotFound} status={404} />
  </Route>
);
