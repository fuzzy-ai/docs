/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { DeleteCross } = require("./icons");
const ErrorBar = React.createClass({
  displayName: "Error Bar",

  getInitialState() {
    return {
      err: this.props.err,
      errorBarOpen: true
    };
  },

  toggleErrorBar() {
    return this.setState({ errorBarOpen: false });
  },

  // coffeelint: disable=max_line_length
  render() {
    let errorBarClass, handleErrorBarToggle;
    const { err } = this.props;
    const { errorBarOpen } = this.state;

    if (errorBarOpen) {
      errorBarClass = "error-bar";
      handleErrorBarToggle = this.toggleErrorBar;
    } else {
      errorBarClass = "error-bar error-bar-is-closed";
      handleErrorBarToggle = this.toggleErrorBar;
    }

    return (
      <div className={errorBarClass} data-have-error={err != null}>
        {err ? (
          <div className="error">
            <span className="close-error-bar" onClick={handleErrorBarToggle}>
              <DeleteCross />
            </span>
            {err.message}
          </div>
        ) : (
          undefined
        )}
      </div>
    );
  }
});

module.exports = ErrorBar;
