/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Docs",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1>PHP</h1>
        <p>
          You can use fuzzy.ai from your PHP project using the Open Source
          library.
        </p>
        <section>
          <h2>Requirements</h2>
          <p>PHP 5.3.3 or later with the cURL extension.</p>
        </section>
        <section>
          <h2>Installation</h2>
          <p>
            You can install the library via{" "}
            <a href="http://getcomposer.org/">Composer</a>:
          </p>
          <pre>{`\
composer require fuzzy-ai\x2Fsdk\
`}</pre>
          <p>To load the library, use Composer's autoload:</p>
          <pre>{`\
require_once(\'vendor\x2Fautoload.php\');\
`}</pre>
        </section>
        <section>
          <h2>Usage</h2>
          <p>
            All API calls require an API key from{" "}
            <a href="https://fuzzy.ai/">https://fuzzy.ai/</a>.
          </p>
          <pre>{`\
$client = new FuzzyAi\\Client(\'YourAPIKey\');

list($result, $evalID) = $client-\x3Eevaluate(\'YourAgentID\', array(\'input1\' =\x3E 42));\
`}</pre>
        </section>
        <section>
          <h2>Examples</h2>
          <p>
            The <code>examples</code> directory has some examples of using the
            library.
          </p>
        </section>
      </div>
    );
  }
});
