/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Docs",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1>RESTful API</h1>
        <p>
          Fuzzy.ai provides a RESTful API so that applications can make
          evaluations remotely.
        </p>
        <p>
          If possible, consider one of the <Link to="/docs/sdk">SDK</Link>{" "}
          libraries provided for your preferred programming language. They are
          much easier to program for than a REST API.
        </p>
      </div>
    );
  }
});
