/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Main logged In Nav",

  render() {
    const step = this.props.tutorial;
    return (
      <ul className="navigation dashboard-nav">
        <li className="navigation__item">
          <a
            href="https://fuzzy.ai/"
            className="navigation__link navigation__link__active"
          >
            Dashboard
          </a>
        </li>
        <li className="navigation__item">
          <Link to="/" className="navigation__link">
            Docs
          </Link>
        </li>
        <li className="navigation__item">
          <a href="http://blog.fuzzy.ai/" className="navigation__link">
            Blog
          </a>
        </li>
        {step != null ? (
          <li className="navigation__item">
            <a
              href={`https://fuzzy.ai/tutorial/${step}`}
              className="navigation__link"
            >
              Finish Tutorial
            </a>
          </li>
        ) : (
          undefined
        )}
      </ul>
    );
  }
});
