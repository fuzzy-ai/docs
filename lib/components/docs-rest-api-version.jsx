/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Docs",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1>API Version</h1>
        <p>
          The fuzzy.ai API version is available via an endpoint. fuzzy.ai uses{" "}
          <a href="http://semver.org/">Semantic versioning</a> for its API
          version numbers. Each version consists of three numbers,{" "}
          <em>major.minor.patch</em>, with this significance:
        </p>
        <ul>
          <li>
            <em>patch</em> numbers are incremented for bug fixes, optimizations,
            or any other change that doesn’t change the API interface. You can
            usually ignore this part, unless you are looking for a particular
            bug fix.
          </li>
          <li>
            <em>minor</em> numbers are for backward-compatible API changes. So
            all API functionality available in version 1.6.0 will also be
            available in version 1.7.0, but some new functionality will be
            available in 1.7.0 that’s not in 1.6.0. Code written for API version
            1.6.0 should work as well, or better, with 1.7.0.
          </li>
          <li>
            <em>major</em> numbers are for non-backwards-compatible API changes.
            Code written for version 1.6.0 will probably not work for version
            2.1.0.
          </li>
        </ul>
        <p>
          <a className="docs" id="getapiversion" name="getapiversion" />
        </p>
        <pre>GET https://api.fuzzy.ai/version</pre>
        <p>This retrieves the current version of the API.</p>
        <h3>Request</h3>
        <p>
          The body of the request is empty. It does not require authentication.
        </p>
        <h3>Response</h3>
        <p>
          The response is UTF-8-encoded JSON representing the version of the API
          currently deployed. It has two properties: <em>name</em> (which is
          always <em>api</em>) and <em>version</em>, which is a Semantic Version
          string for the version. For example:
        </p>
        <pre>{`\
{ \n \
"name": "api", \n \
"version": "0.7.0" \n \
}\
`}</pre>
        <p>
          Note: comparing semantic version numbers can be tricky; you should
          check for a semantic-version-parsing library for your programming
          language.
        </p>
      </div>
    );
  }
});
