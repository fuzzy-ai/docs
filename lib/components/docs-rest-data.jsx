/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Docs",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1>Data</h1>
        <p>
          {`For fuzzy.ai, the body of all requests, and all their
responses, are  `}
          <a href="http://en.wikipedia.org/wiki/UTF-8">UTF-8</a>-encoded{" "}
          <a href="http://en.wikipedia.org/wiki/JSON">JSON</a>
          {`.
JSON is the format used in JavaScript code for objects, arrays,
numbers and string literals. It’s used by a lot of APIs and
supported by libraries in hundreds of programming languages.
For more information on how to encode and decode JSON in your
programming language of choice, see  `}
          <a href="http://json.org/">http://json.org/</a>
        </p>
      </div>
    );
  }
});
