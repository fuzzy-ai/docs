/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");
const FuzzyVcard = require("./fuzzy-vcard");
const FuzzyFooterNav = require("./fuzzy-footer-nav");
const FuzzySocialFooterBar = require("./fuzzy-social-footer-bar");
module.exports = React.createClass({
  displayName: "Footer",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <footer className="footer">
        <div className="footer-inner">
          <div className="page">
            <FuzzyVcard />
            <FuzzyFooterNav />
            <div className="copyright">
              <p className="copyright__content upper">
                Copyright © 2014–{new Date().getFullYear()} fuzzy.ai. All rights
                reserved.
              </p>
            </div>
          </div>
        </div>
        <FuzzySocialFooterBar />
      </footer>
    );
  }
});
