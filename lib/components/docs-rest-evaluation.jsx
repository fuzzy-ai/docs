/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Docs",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1 className="sub-header">Evaluation and feedback</h1>
        <p>{`The inference endpoint returns an evaluation ID that can be
used later to get details on the evaluation and provide
feedback.`}</p>
        <pre>
          GET https://api.fuzzy.ai/evaluation/<em>evaluation id</em>
        </pre>
        <p>This retrieves the details of an evaluation.</p>
        <h2>Request</h2>
        <p>The body of the request is empty.</p>
        <h2>Response</h2>
        <p>{`The response is UTF-8-encoded JSON representing evaluation
log. It has the following properties.`}</p>
        <table cellPadding="0" cellSpacing="0">
          <tbody>
            <tr>
              <th className="upper fwb">Property</th>
              <th className="upper fwb">Purpose</th>
              <th className="upper fwb">Example</th>
            </tr>
            <tr>
              <td data-title="Property">
                <p>reqID</p>
              </td>
              <td data-title="Purpose">
                <p>{`Unique ID of the
evaluation; an opaque string`}</p>
              </td>
              <td data-title="Example">
                <pre>{`\
\\n\"546b6df3-5ed4-48ce-8d06-efa1fb7fcf22\"\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>userID</p>
              </td>
              <td data-title="Purpose">
                <p>{`Unique ID of the user;
an opaque string`}</p>
              </td>
              <td data-title="Example">
                <pre>{`\
\\n\"vb5RR1H2c0\"\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>agentID</p>
              </td>
              <td data-title="Purpose">
                <p>Unique ID of the agent; an opaque string</p>
              </td>
              <td data-title="Example">
                <pre>{`\
\\n\"MM1134viO8\"\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>agentID</p>
              </td>
              <td data-title="Purpose">
                <p>Unique ID of the agent version; an opaque string</p>
              </td>
              <td data-title="Example">
                <pre>{`\
\\n\"Z3c3l7awq\"\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>input</p>
              </td>
              <td data-title="Purpose">
                <p>Object mapping input names to values</p>
              </td>
              <td data-title="Example">
                <pre>{`\
{ \n \
"temperature": 18.9, \n \
"pressure": 177 \n \
\n}`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>fuzzified</p>
              </td>
              <td data-title="Purpose">
                <p>{`Degrees of membership
of each input value in the fuzzy sets defined for
that input.`}</p>
              </td>
              <td data-title="Example">
                <pre>{`{ \n \
"temperature": { \n \
"cool": 0.4, \n \
"warm": 0.5 \n \
}, \n \
"pressure": { \n \
"moderate": 0.8 \n \
"high": 0.1 \n \
}\n \
}`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>rules</p>
              </td>
              <td data-title="Purpose">
                <p>
                  Array of strings representing rules that contributed to the
                  output.
                </p>
              </td>
              <td data-title="Example">
                <pre>{`\
[
  \"IF temperature IS cool THEN fanSpeed IS moderate\",
  \"IF temperature IS warm THEN fanSpeed IS moderateFast\",
  \"IF pressure IS moderate THEN fanSpeed IS slow\",
  \"IF pressure IS high THEN fanSpeed IS verySlow\"
]\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>inferred</p>
              </td>
              <td data-title="Purpose">
                <p>{`Object representing
fuzzy membership values of output variables`}</p>
              </td>
              <td data-title="Example">
                <pre>{`\
{\n \
"fanSpeed": { \n \
"moderate": 0.4,\n \
"moderateFast": 0.5,\n \
"slow": 0.8\n \
"verySlow": 0.1\n \
}\n \
}\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>clipped</p>
              </td>
              <td data-title="Purpose">
                <p>
                  Fuzzy sets shapes clipped to the degree of membership of the
                  output values
                </p>
              </td>
              <td data-title="Example">
                <pre>{`\
{ \n \
"fanSpeed": { \n \
"verySlow": [[0, 0], [12, 0.1], [14, 0.1], [20, 0]] \n \
"slow": [[6, 0], [20, 0.8], [30, 0.8], [40, 0]] \n \
"moderate": [[35, 0], [50, 0.4], [60, 0.4], [70, 0]] \n \
"moderateFast": [[60, 0], [80, 0.5], [100, 0.5], [120, 0]] \n \
}\n \
}\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>combined</p>
              </td>
              <td data-title="Purpose">
                <p>
                  Minimal combined polygons combining all the polygons in
                  'clipped'
                </p>
              </td>
              <td data-title="Example">
                <pre>{`\
{ \
"fanSpeed": [ \
[0, 0], \
[8, 0.43], \
[20, 0.8], \
[30, 0.8], \
[38, 0.2], \
[50, 0.4], \
[60, 0.4], \
[68, 0.2], \
[8, 0.5], \
[100, 0.5], \
[120, 0]] \
] \
}\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>centroid</p>
              </td>
              <td data-title="Purpose">
                <p>Centroid of the polygons in 'combined'</p>
              </td>
              <td data-title="Example">
                <pre>{`\
{ \n \
"fanSpeed": [50, 0.33]\n \
}\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>crisp</p>
              </td>
              <td data-title="Purpose">
                <p>Eventual defuzzified output</p>
              </td>
              <td data-title="Example">
                <pre>{`\
{ \n \
"fanSpeed": 50\n \
}\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>createdAt</p>
              </td>
              <td data-title="Purpose">
                <p>Timestamp for evaluation</p>
              </td>
              <td data-title="Example">
                <pre>{`\
\"2015-05-31T00:14:01Z\"\
`}</pre>
              </td>
            </tr>
          </tbody>
        </table>
        <pre>
          {" "}
          POST https://api.fuzzy.ai/evaluation/<em>
            evaluation id
          </em>/feedback{" "}
        </pre>
        <p>
          This creates feedback for a particular evaluation. This is the main
          mechanism for optimizing a fuzzy agent.
        </p>
        <p>
          Each feedback post can included multiple scores showing the success
          (or failure) of the evaluation. The names and scales of these scores
          is up to you. The scales are also independent of each other.
        </p>
        <h2>Request</h2>
        <p>
          The body of the request is UTF-8-encoded JSON representing the
          feedback.
        </p>
        <table cellPadding="0" cellSpacing="0">
          <tbody>
            <tr>
              <th className="fwb upper">Property</th>
              <th className="fwb upper">Purpose</th>
              <th className="fwb upper">Example</th>
            </tr>
            <tr>
              <td data-title="Property">
                <p>data</p>
              </td>
              <td data-title="Purpose">
                <p>Object mapping success metric names to values</p>
              </td>
              <td data-title="Example">
                <pre>{`\
{ \n \
"airFlow": 116,\n \
"ambientTemperature": 20\n \
}\
`}</pre>
              </td>
            </tr>
          </tbody>
        </table>
        <h3>Response</h3>
        <p>{`The response is UTF-8-encoded JSON representing the
feedback. It has the following properties.`}</p>
        <table cellPadding="0" cellSpacing="0">
          <tbody>
            <tr>
              <th className="upper fwb">Property</th>
              <th className="upper fwb">Purpose</th>
              <th className="upper fwb">Example</th>
            </tr>
            <tr>
              <td data-title="Property">
                <p>id</p>
              </td>
              <td data-title="Purpose">
                <p>Unique ID for the feedback; an opaque string</p>
              </td>
              <td data-title="Example">
                <pre>{`\
\"0292368a-0b27-11e5-8964-c8f73398600c\"\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>evaluationLog</p>
              </td>
              <td data-title="Purpose">
                <p>Unique ID for the evaluation; an opaque string</p>
              </td>
              <td data-title="Example">
                <pre>{`\
\"1cc72b46-0b27-11e5-8964-c8f73398600c\"\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>data</p>
              </td>
              <td data-title="Purpose">
                <p>Object mapping success metric names to values</p>
              </td>
              <td data-title="Example">
                <pre>{`\
{ \n \
"airFlow": 116, \n \
"ambientTemperature": 20 \n \
}\
`}</pre>
              </td>
            </tr>
            <tr>
              <td data-title="Property">
                <p>createdAt</p>
              </td>
              <td data-title="Purpose">
                <p>Timestamp for the feedback</p>
              </td>
              <td data-title="Example">
                <pre>{`\
\"2015-06-01T08:31:06Z\"\
`}</pre>
              </td>
            </tr>
          </tbody>
        </table>
        <pre>
          GET https://api.fuzzy.ai/evaluation/<em>evaluation id</em>/feedback
        </pre>
        <p>This shows all feedback for a particular evaluation.</p>
        <h2>Request</h2>
        <p>The body of the request is empty.</p>
        <h2>Response</h2>
        <p>
          {`The response is UTF-8-encoded JSON representing the
feedback. It is an array of feedback objects as described in
the\&nbps;`}
          <a href="#postfeedback">{`POST
\x2Fevaluation\x2F\x3Cevaluation id\x3E\x2Ffeedback`}</a>
          {` endpoint
documentation.`}
        </p>
      </div>
    );
  }
});
