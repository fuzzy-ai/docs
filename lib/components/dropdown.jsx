/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { PlusIcon } = require("./icons");

const Dropdown = React.createClass({
  displayName: "dropdown",

  getInitialState() {
    return { showDropdown: false };
  },

  toggleDropdown(e) {
    e.preventDefault();
    return this.setState({ showDropdown: !this.state.showDropdown });
  },

  handleClick(e) {
    e.preventDefault();
    const value = e.target.getAttribute("data-value");
    if (typeof this.props.selectHandler === "function") {
      this.props.selectHandler(value);
    }
    return this.setState({ showDropdown: false });
  },

  // coffeelint: disable=max_line_length
  render() {
    let dropdownButtonClass, handleToggle;
    const { title, options } = this.props;
    const { showDropdown } = this.state;
    const { handleClick } = this;

    if (showDropdown) {
      dropdownButtonClass =
        "dropdown-title dropdown-title-inline dropdown-title--is-on";
      handleToggle = this.toggleDropdown;
    } else {
      dropdownButtonClass = "dropdown-title dropdown-title-inline";
      handleToggle = this.toggleDropdown;
    }

    return (
      <span className="dropdown dropdown-inline-rules">
        <span className={dropdownButtonClass} onClick={handleToggle}>
          {title}
          <span className="dropdown__plus-icon">
            <PlusIcon />
          </span>
        </span>
        {this.state.showDropdown ? (
          <ul className="dropdown-options dropdown-inline-rules__options">
            {options.map((option, i) => (
              <li key={i}>
                <a data-value={option.value} onClick={handleClick}>
                  {option.label}
                </a>
              </li>
            ))}
          </ul>
        ) : (
          undefined
        )}
      </span>
    );
  }
});

module.exports = Dropdown;
