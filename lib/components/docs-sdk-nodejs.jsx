/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Node.js SDK",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1>Node.js SDK</h1>
        <p>
          fuzzy.ai has an official library for  <a href="http://nodejs.org/">
            Node.js
          </a>
          {`.
It\'s available under the `}
          <a href="http://www.apache.org/licenses/LICENSE-2.0">
            Apache Public License 2.0.
          </a>
          {`and should be usable in commercial and Open
Source projects.`}
        </p>
        <h2>Installation</h2>
        <p>The fuzzy.ai package on npm has the latest version of the code:</p>
        <p>
          <a href="https://www.npmjs.com/package/fuzzy.ai">
            https://www.npmjs.com/package/fuzzy.ai
          </a>
        </p>
        <p>You can use npm to install:</p>
        <pre>npm install fuzzy.ai</pre>
        <p>… or add it to your package.json file.</p>
        <p>You can also fork the repository on Github.</p>
        <p>
          <a href="https://github.com/fuzzy-ai/nodejs">
            https://github.com/fuzzy-ai/nodejs
          </a>
        </p>
        <p>… or download the latest release:</p>
        <p>
          <a href="https://github.com/fuzzy-ai/nodejs/releases">
            https://github.com/fuzzy-ai/nodejs/releases
          </a>
        </p>
        <h2>Overview</h2>
        <pre>
          {`\
var FuzzyAIClient = require(\'fuzzy.ai\');

var apiKey = \'API key from fuzzy.ai\';

var client = new FuzzyAIClient(apiKey);

var agentID = \'ID from fuzzy.ai\';

var inputs = new Object();

inputs[\'temperature\'] = 87;

client.evaluate(agentID, inputs, function(err, outputs)`}
          {` {\n \
if (err) { \n \
console.error(err);\n \
} else {\n \
console.log("Fan speed is " + outputs.fanSpeed);\n \
}\n \
});`}
        </pre>
        <h2>FuzzyAIClient</h2>
        <p>This is the main class; it's what's returned from the require().</p>
        <p>
          <strong>FuzzyAIClient(apiKey, apiRoot)</strong> You have to get an{" "}
          <code>apiKey</code> from http://fuzzy.ai/ . Keep this secret, by the
          way. <code>serverRoot</code> is the root of the API server. It has the
          correct default 'https://api.fuzzy.ai' but if you're doing some
          testing with a mock, it can be useful.
        </p>
        <p>This is the main method you need to use:</p>
        <p>
          <strong>evaluate(agentID, inputs, callback)</strong> Does a single
          inference. <code>agentID</code> is on the main page for the agent on
          http://fuzzy.ai/ . The <code>inputs</code> is an object, mapping input
          names to numeric values. <code>callback</code> is a function with the
          signature <code>function(err, outputs)</code>, where{" "}
          <code>outputs</code> is an object mapping output names to numeric
          values.
        </p>
        <p>
          These might be useful but you normally don't need to mess with them.
        </p>
        <ul>
          <li>
            <strong>getAgents(callback)</strong> <code>userID</code> is the user
            ID, <em>not</em> the API key. <code>callback</code> is a function
            with the signature <code>function(err, agents)</code>, where{" "}
            <code>agents</code> is an array of objects with <code>id</code> and{" "}
            <code>name</code>
            {` properties, one for each agent the user has.\
`}
          </li>
          <li>
            <strong>newAgent(agent, callback)</strong> <code>userID</code> is
            the user ID. <code>agent</code> is an agent object with at least
            properties <code>inputs</code>, <code>outputs</code>,{" "}
            <code>rules</code>. <code>callback</code> is a function with the
            signature <code>function(err, agent)</code>
            {` which returns the fully-realized agent with all its properties like timestamps and IDs.\
`}
          </li>
          <li>
            <strong>getAgent(agentID, callback)</strong> Gets a single agent by
            ID. <code>callback</code> is a function with the signature{" "}
            <code>function(err, agent)</code>
            {`.\
`}
          </li>
          <li>
            <strong>putAgent(agentID, agent, callback)</strong> Updates an
            agent. <code>callback</code> has the signature{" "}
            <code>function(err, agent)</code>
            {` which will return the updated version.\
`}
          </li>
        </ul>
      </div>
    );
  }
});
