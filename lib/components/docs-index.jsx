/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "DocsIndex",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div className="main">
        <h1 className="fwb">Documentation</h1>
      </div>
    );
  }
});
