/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const Radium = require("radium");

let ListItem = React.createClass({
  displayName: "ListItem",

  handleClick(e) {
    e.preventDefault();
    return this.props.handleClick(this.props.value);
  },

  getDefaultProps() {
    return {
      arrowStyle: {
        display: "block",
        position: "absolute",
        top: "-1px",
        right: "50%",
        left: "50%",
        width: "0px",
        height: "0px"
      },

      listStyle: {
        padding: "5px 0",
        margin: "0px 0px 1px 0px",
        textAlign: "center",
        fontWeight: "normal",
        background: "#fffef7",
        display: "block",
        fontSize: "12px",
        color: "#85649c",
        ":hover": {
          boxShadow: "inset 0 0 0 100px #FAFBF3",
          color: "#473d4e",
          transitionDuration: ".3s",
          transitionTimingFunction: "ease-in"
        }
      }
    };
  },

  // coffeelint: disable=max_line_length
  render() {
    return (
      <li onClick={this.handleClick} style={this.props.listStyle}>
        <em style={this.props.arrowStyle} />
        {this.props.value}
      </li>
    );
  }
});

ListItem = Radium(ListItem);

const Autocomplete = React.createClass({
  displayName: "Autocomplete",

  getDefaultProps() {
    return {
      wrapperStyle: {
        position: "relative",
        zIndex: 10000
      },
      ulStyle: {
        background: "#dce0e4",
        padding: "0px",
        border: "1px solid #dce0e4",
        boxShadow: "0px 4px 10px -10px rgba(55,55,55, 0.5)",
        fontSize: "12px",
        position: "absolute",
        minHeight: "100%",
        width: "100%",
        top: "100%",
        zIndex: "999"
      }
    };
  },

  getInitialState() {
    return {
      showList: false,
      highlightedIndex: null
    };
  },

  renderItems() {
    const { handleClick } = this;
    const showItems = this.searchItems(this.props.items, this.props.value);

    if (!showItems.length) {
      return;
    }

    const children = [];
    showItems.map((item, i) =>
      children.push(<ListItem key={i} handleClick={handleClick} value={item} />)
    );

    return (
      <div
        className="autocomplete-wrapper-inner"
        style={this.props.wrapperStyle}
      >
        <ul style={this.props.ulStyle}>{children}</ul>
      </div>
    );
  },

  searchItems(items, value) {
    if (!items) {
      return [];
    }

    const searchTerm = new RegExp(value, "i");
    const results = [];
    items.map(function(item, i) {
      if (searchTerm.exec(item)) {
        return results.push(item);
      }
    });
    return results;
  },

  handleFocus(e) {
    return this.setState({ showList: true });
  },

  handleBlur(e) {
    if (this.props.onBlur) {
      const options = this.searchItems(this.props.items, e.target.value);
      return this.props.onBlur(e.target.value, options);
    }
  },

  handleClick(value) {
    this.props.onChange(value);
    return this.setState({ showList: false });
  },

  handleChange(e) {
    return this.props.onChange(e.target.value);
  },

  render() {
    return (
      <div className="autocomplete-wrapper">
        <input
          type="text"
          role="combobox"
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          onChange={this.handleChange}
          value={this.props.value}
          placeholder={this.props.placeholder}
          required={true}
        />
        {this.state.showList && this.renderItems()}
      </div>
    );
  }
});

const styles = {};

module.exports = Radium(Autocomplete);
