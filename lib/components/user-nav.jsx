/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");

module.exports = React.createClass({
  displayName: "User Nav",

  render() {
    return (
      <div className="login-menu-not">
        <ul className="login-nav">
          <li className="login-nav__item">
            <a href="https://fuzzy.ai/signup" className="signup user-action">
              Get Started
            </a>
          </li>
          <li className="login-nav__item">
            <a href="https://fuzzy.ai/login" className="login user-action">
              Log in
            </a>
          </li>
        </ul>
      </div>
    );
  }
});
