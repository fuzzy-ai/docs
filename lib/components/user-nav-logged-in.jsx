/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "User Nav logged In",

  render() {
    <span className="user-detail__email line">{email}</span>;
    return (
      <span className="user-detail__plan line">
        Current plan: <em>{plan}</em>
      </span>
    );
  }
});
