/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");

module.exports = React.createClass({
  displayName: "Clipboard",

  componentDidMount() {
    if (document) {
      let clipboard = require("clipboard");
      clipboard =
        typeof clipboard === "function"
          ? new clipboard(document.querySelectorAll(".copy-paste"))
          : undefined;
      return;
    }
  },

  // coffeelint: disable=max_line_length
  render() {
    const { copyText } = this.props;
    return React.createElement(
      "span",
      { className: "num dashboard-api__key spaced-medium" },
      copyText,
      React.createElement(
        "a",
        {
          onClick(e) {
            return e.preventDefault();
          },
          className: "copy-paste",
          "data-clipboard-text": copyText
        },
        <svg
          data-name="Layer 1"
          xmlns="http:#www.w3.org/2000/svg"
          viewBox="0 0 288 384"
          className="copy-paste__svg"
        >
          <path d="M32 352V48H0v336h240v-32H32" />
          <path d="M213.3 0H48v336h240V75zM256 112h-80V32h16v64h64v16z" />
        </svg>
      )
    );
  }
});
