/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "page title",

  render() {
    return (
      <div className="row-inner-section-hero">
        <main className="page-intro-zone page">
          <header className="main-intro-text">
            <h1 className="page-title-wrap__heading inline big-title fwb">
              {this.props.title}
            </h1>
            <h2 className="big-title light-color inline fwl">
              {" "}
              {this.props.subtitle}
            </h2>
            <p className="big-para">{this.props.desc1}</p>
            {this.props.desc2 ? (
              <p className="big-para">{this.props.desc2}</p>
            ) : (
              undefined
            )}
            {this.props.desc3 ? (
              <p className="big-para">{this.props.desc3}</p>
            ) : (
              undefined
            )}
          </header>
        </main>
      </div>
    );
  }
});
