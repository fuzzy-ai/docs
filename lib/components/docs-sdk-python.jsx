/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Docs",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1>Python</h1>
        <p>
          You can use fuzzy.ai from your Python project using the Open Source
          library.
        </p>
        <section>
          <h2>Installation</h2>
          <p>
            You can download the software at:{" "}
            <a href="https://github.com/fuzzy-ai/python">
              https://github.com/fuzzy-ai/python
            </a>
          </p>
          <p>You can also fork the repository on Github.</p>
        </section>
        <section>
          <h2>Testing</h2>
          <p>Testing uses tox and supports py27, py33, py34, py35 and pypy.</p>
          <p>To run for a single version:</p>
          <pre>{`\
tox -e py27\
`}</pre>
        </section>
        <section>
          <h2>Basic usage</h2>
          <p>
            When you use the fuzzyai module, you always have to provide your API
            key first. Use the setup() function to do that:
          </p>
          <pre>{`\
from fuzzyai.client import Client

client = Client(YOUR_API_KEY)\
`}</pre>
          <p>
            To have a Fuzzy.ai agent make a decision for you, use the evaluate()
            function of the fuzzyai module:
          </p>
          <pre>{`\
agent_id = \"AGENTIDHERE\"

inputs = dict(height=188, weight=88.7)

(outputs, evaluation_id) = client.evaluate(agent_id, inputs)

# Real-world usage of the run_distance will return some performance
# metric.

client.feedback(evaluation_id, dict(weight_loss=0.25))\
`}</pre>
        </section>
      </div>
    );
  }
});
