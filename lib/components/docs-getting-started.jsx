/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");
const { connect } = require("react-redux");

const Btn = require("./btn");

const DocsGetStarted = React.createClass({
  displayName: "DocsIndex",

  // coffeelint: disable=max_line_length
  render() {
    const { authenticated } =
      (this.props != null ? this.props.user : undefined) != null;
    return (
      <div>
        <h1>Getting Started</h1>
        <p>
          fuzzy.ai is an API that lets any developer easily add machine
          intelligence to their working code. fuzzy.ai lets you deploy fuzzy
          agents in the cloud that can help you make decisions automatically.
        </p>
        {authenticated ? (
          <Btn
            link="/tutorial"
            text="Try the Tutorial"
            class="btn btn__regular"
          />
        ) : (
          undefined
        )}
        <br />
        <h2>What is a fuzzy agent?</h2>
        <p>
          A fuzzy agent is a virtual intelligent machine that helps your code
          make complex decisions. The agent works like this:
        </p>
        <p className="text-center">
          <img
            className="img-responsive"
            src="/assets/i/fuzzy-control-diagram.png"
          />
        </p>
        <ol>
          <li>
            It receives one or more named input values, like ‘temperature’ or
            ‘numberOfShares’. Each value must be a number, like 80 or -1000.5.
          </li>
          <li>
            It converts these numbers into fuzzy set membership. For each fuzzy
            set you define, it determines the degree of membership of the input
            in the set. So, a temperature of 80°F might be about 70% “Pleasant”
            and about 20% “Hot”. This process is called <em>fuzzification</em>.
          </li>
          <li>
            It uses the rules you provide to do fuzzy inference on the input. If
            the agent has rules like “If the temperature is Pleasant, then the
            AirConditioning is Off” and “IF the temperature is Hot, then the
            Air-Conditioning is High”, then the AirConditioning fuzzy output
            will be 70% “Off” and 20% “High” for 80°F input value.
          </li>
          <li>
            Finally, it converts the fuzzy output into “crisp” numbers. The
            fuzzy output memberships are balanced out geometrically to find a
            good number that blends them all together. This process is called{" "}
            <em>defuzzification</em>. The crisp numbers are then returned to the
            caller as output.
          </li>
        </ol>
        <h2>When should you use fuzzy logic?</h2>
        <p>
          Fuzzy logic is great for certain kinds of applications. Fuzzy logic
          works great when…{" "}
        </p>
        <ul>
          <li>
            <strong>… You already know your business rules. </strong>If you know
            that you want to give a big discount to new customers, and little to
            no discount to regular customers, then you don’t need a machine
            learning algorithm to derive that rule. Instead, you can get right
            to implementing it in fuzzy logic. You can always add new rules to a
            fuzzy agent; you don’t have to know <em>all</em> your rules before
            you get started.
          </li>
          <li>
            <strong>
              … Your business rules use common-sense, qualitative terms.{" "}
            </strong>If you have a rule that a <em>good</em> sales
            representative should get a <em>big</em> bonus, then your rules are
            qualitative. They use common-sense intuitive terms that regular
            people understand. You can turn terms like <em>good</em> and{" "}
            <em>big</em> into fuzzy sets so that your fuzzy agent can understand
            them, too.
          </li>
          <li>
            <strong>… Your rules can be contradictory. </strong>If you have one
            rule that says to give a high priority to sales prospects interested
            in your new product, and another rule that says to give a low
            priority to sales prospects with low annual revenue, what do you do
            when a low-revenue prospect is interested in your new product? Fuzzy
            agents are good at balancing contradictory rules to come up with
            reasonable compromises.
          </li>
          <li>
            <strong>… You want to add, modify and remove rules easily. </strong>Because
            the fuzzy agents balance contradictory rules, you can easily add
            more rules, remove rules, or modify existing rules. As you develop
            more understanding of your problem space, or you have a changing
            situation on the ground, your fuzzy agent can change and adapt also.
          </li>
          <li>
            <strong>… You have numerical input. </strong>Fuzzy logic only works
            with numbers as inputs. Sometimes it’s hard to see the numbers in
            your problems, but usually it’s easy. Let’s say you’re trying to
            figure out whether a salesman has been performing well in order to
            determine their monthly bonus. A number that you could use as input
            to the fuzzy agent is their sales revenue as a percentage of their
            sales quota. Or if you want to measure if a Tweet is about your
            company, you can use a regular expression to count the number of
            times your company name, initials, Twitter handle or domain name are
            mentioned in the text.
          </li>
          <li>
            <strong>… You want to see how it works. </strong>The internal
            workings of your fuzzy agent, although complex, are relatively easy
            to audit. Because it’s using the fuzzy sets and fuzzy rules you
            provide, most of its decisions will “make sense” quickly. For those
            that aren’t obvious, you can review the intermediate values to
            determine why.
          </li>
          <li>
            <strong>… You need to deploy quickly. </strong>Setting up a fuzzy
            agent with a half-dozen or so inputs and a few outputs should take
            under an hour. Integrating that fuzzy agent with your code can take
            only a few minutes. You can get good-quality intelligence working in
            your application very quickly.
          </li>
        </ul>
        <h2>Next steps</h2>
        <p>
          To get a fuzzy agent working in your software, you need to follow
          these steps.
        </p>
        <ol>
          <li>
            <strong>Create a new fuzzy agent. </strong>Use the New Agent tool to
            create and name your fuzzy agent. It will start off with an ID and
            some sample rules and input and output fuzzy sets.
          </li>
          <li>
            <strong>Configure your fuzzy agent. </strong>
            {`You have to configure your fuzzy agent to solve your particular business problems.\
`}
            <ol>
              <li>
                <strong>Define your input variables. </strong> Pick the input
                variables you’ll use for your fuzzy agent. They should be
                numeric values. You need to define fuzzy sets that map input
                values into qualitative categories like “Hot” or “Interested” or
                “Old”.
              </li>
              <li>
                <strong>Define your output variables. </strong> What do you want
                to find out? You can define one or a few output variables. These
                have fuzzy sets too -- but they work in reverse, taking
                qualitative outputs from the fuzzy inference and{" "}
                <em>defuzzifying</em> them into crisp numbers.
              </li>
              <li>
                <strong>Define your fuzzy rules. </strong> Each rule takes an
                input fuzzy set -- like when a temperature is hot -- and maps it
                to an output fuzzy set, like when the air conditioning is on
                high. Usually, these fuzzy rules are really easy to figure out
                -- they feel like common sense.
              </li>
            </ol>
          </li>
          <li>
            <strong>Test your fuzzy agent. </strong>You can test your fuzzy
            agent using the Web interface at fuzzy.ai. Try a few different
            values for your different inputs -- high and low, complementary and
            contradictory. If the output seems wrong, try adjusting your fuzzy
            sets to make sure they make sense.
          </li>
          <li>
            <strong>Call your fuzzy agent from your application code. </strong>
            {`This is the last step!\
`}
            <ol>
              <li>
                <strong>
                  Install the fuzzy.ai library for your programming language.
                </strong>{" "}
                There are Open Source libraries for many programming languages
                available at fuzzy.ai, on fuzzy.ai’s Github account, or from the
                package repository for your language. If there’s no library for
                your language, use the (simple) REST API directly.
              </li>
              <li>
                <strong>Prepare your input values. </strong> In your code,
                prepare the input values you’re going to pass to the fuzzy
                agent. Some may come from your database; others might be entered
                by the user. Still others may be calculated based on other
                non-numeric data.
              </li>
              <li>
                <strong>Call the fuzzy agent from your code. </strong> Using
                your API key and the agent ID, pass the input to the fuzzy
                agent, and get the output values it returns.
              </li>
              <li>
                <strong>Integrate the output. </strong> The output values may
                help you sort database records, or you may show them directly to
                the user. Or they may be terms in a larger calculation.{" "}
              </li>
              <li>
                NOTE: fuzzy.ai is a remote API, and a lot can go wrong between
                your computer and your fuzzy agent. You should try to have a
                sensible default to fall back to for any calculation you pass to
                a fuzzy agent.{" "}
              </li>
            </ol>
          </li>
          <li>
            <strong>Modify your agent as needed.</strong> Often, you can change
            the fuzzy sets or fuzzy rules in your fuzzy agent without any change
            to your application code. If you add or remove input or output
            variables, though, you’ll need to handle those changes in your
            application code, too.{" "}
          </li>
        </ol>
      </div>
    );
  }
});

const mapStateToProps = state => ({
  user: state.user
});

module.exports = connect(mapStateToProps)(DocsGetStarted);
