/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Footer",

  render() {
    return (
      <div className="row">
        <div className="main">
          <h1>Error</h1>
          <p>ERROR MESSAGE</p>
        </div>
      </div>
    );
  }
});
