/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

const DocsSidebarNav = require("./docs-sidebar-nav");
const DocsIndex = require("./docs-index");

const Docs = React.createClass({
  displayName: "Docs",

  render() {
    return (
      <div className="row">
        <div className="docs-wrapper page">
          <DocsSidebarNav />
          <div className="docs-content">{this.props.children}</div>
        </div>
      </div>
    );
  }
});

module.exports = Docs;
