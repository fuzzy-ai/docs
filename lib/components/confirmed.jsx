/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Confirmed",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div className="row site-wrap">
        <main className="page-intro-zone page">
          <header className="main-intro-text">
            <h1 className="page-title-wrap__heading inline big-title fwb">
              Signup complete
            </h1>
            <h2 className="big-title light-color inline fwl mbl">
              {" "}
              Thanks for signing up. Fuzzy.ai is a great way to build artificial
              intelligence into your applications.
            </h2>
            <br />
            <br />
            <p>
              In the following{" "}
              <strong>
                <Link to="/tutorial">tutorial</Link>
              </strong>, we’ll show you how to use <strong>Fuzzy.ai</strong> to
              offer dynamic prices at a lemonade stand. It should take only a
              few minutes and will introduce you to the{" "}
              <strong>features of Fuzzy.ai</strong>
              {`.\
`}
            </p>
          </header>
          <div className="pagination tuts-pagination">
            <Link to="/tutorial" className="btn btn-blue">
              Go to step one: <strong>understanding the problem</strong>
            </Link>
            <br />
            <Link to="/agents/new" className="secondary-tut-link">
              Skip tutorial
            </Link>
          </div>
        </main>
      </div>
    );
  }
});
