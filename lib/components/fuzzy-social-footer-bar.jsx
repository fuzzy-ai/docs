/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");
const { Facebook, Twitter, LinkedIn, AngelCo } = require("./icons");

module.exports = React.createClass({
  displayName: "fuzzy social footer",

  componentDidMount() {
    const backtoTop = document.querySelector(".backtotop");
    const app = document.querySelector("#app");

    const scrollUp = function(e) {
      e.preventDefault();
      app.scrollIntoView({
        behavior: "smooth"
      });
    };

    return backtoTop != null
      ? backtoTop.addEventListener("click", scrollUp)
      : undefined;
  },

  render() {
    return (
      <div className="footer_end">
        <div className="social-links-wrapper">
          <ul className="social-links">
            <li className="social-links__items">
              <div className="icon icon-twitter">
                <Twitter
                  link="https://www.twitter.com/fuzzyai"
                  share="twitter"
                  shareUrl="https://twitter.com/share?url="
                  width={24}
                  height={24}
                />
              </div>
            </li>
            <li className="social-links__items">
              <div className="icon icon-facebook">
                <Facebook
                  link="https://www.facebook.com/fuzzy.io"
                  share="facebook"
                  shareUrl="https://www.facebook.com/fuzzy.io"
                  width={24}
                  height={24}
                />
              </div>
            </li>
            <li className="social-links__items">
              <div className="icon icon-angel">
                <AngelCo
                  link="https://angel.co/fuzzy-ai"
                  share="Angel Co"
                  shareUrl="https://angel.co/fuzzy-ai"
                  width={24}
                  height={24}
                />
              </div>
            </li>
            <li className="social-links__items">
              <div className="icon icon-linkedin">
                <LinkedIn
                  link="https://www.linkedin.com/company/fuzzy-io"
                  share="LinkedIn"
                  shareUrl="https://www.linkedin.com/company/fuzzy-io"
                  width={24}
                  height={24}
                />
              </div>
            </li>
          </ul>
        </div>
        <span className="backtotop">
          <a href="#" onClick={this.scrollUp} />
        </span>
      </div>
    );
  }
});
