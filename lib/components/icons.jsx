/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

//Footer icons with Links not for about page

exports.Twitter = React.createClass({
  displayName: "Twitter",

  render() {
    const { height, width, link, dataShare, dataSharingUrl } = this.props;
    return (
      <a
        href={link}
        className="social-links__items-anchor"
        data-starting-url={dataSharingUrl}
        data-share={dataShare}
        target="_blank"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 512 512"
          width={width}
          height={height}
          className="icon__svg"
          aria-labelledby="title"
        >
          <use xlinkHref="/assets/sprite.svg#twitter" />
        </svg>
      </a>
    );
  }
});

exports.Facebook = React.createClass({
  displayName: "Facebook",

  render() {
    const { height, width, link, dataShare, dataSharingUrl } = this.props;
    return (
      <a
        href={link}
        className="social-links__items-anchor"
        data-starting-url={dataSharingUrl}
        data-share={dataShare}
        target="_blank"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 512 512"
          className="icon__svg"
        >
          <use xlinkHref="/assets/sprite.svg#facebook" />
        </svg>
      </a>
    );
  }
});

exports.LinkedIn = React.createClass({
  displayName: "LinkedIn",

  render() {
    const { height, width, link, dataShare, dataSharingUrl } = this.props;
    return (
      <a
        href={link}
        className="social-links__items-anchor"
        data-starting-url={dataSharingUrl}
        data-share={dataShare}
        target="_blank"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="icon__svg"
          viewBox="0 0 430.12 411"
        >
          <use xlinkHref="/assets/sprite.svg#linkedin" />
        </svg>
      </a>
    );
  }
});

exports.AngelCo = React.createClass({
  displayName: "AngelCo",

  render() {
    const { height, width, link, dataShare, dataSharingUrl } = this.props;
    return (
      <a
        href={link}
        className="social-links__items-anchor"
        data-starting-url={dataSharingUrl}
        data-share={dataShare}
        target="_blank"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="angel icon__svg i"
          viewBox="0 0 21.9 31.4"
        >
          <use xlinkHref="/assets/sprite.svg#angelco" />
        </svg>
      </a>
    );
  }
});

exports.GearIcon = React.createClass({
  displayName: "Gear Icon",

  render() {
    return (
      <svg viewBox="0 0 32 32" className="ui-icon  gear-icon ">
        <use xlinkHref="/assets/sprite.svg#icon-cog" />
      </svg>
    );
  }
});

exports.StatsIcon = React.createClass({
  displayName: "Stats Icon",

  render() {
    return (
      <svg viewBox="0 0 32 32" className="stats-icon ui-icon">
        <use xlinkHref="/assets/sprite.svg#icon-stats-bars" />
      </svg>
    );
  }
});

exports.EditIcon = React.createClass({
  displayName: "Edit Icon",

  render() {
    return (
      <svg viewBox="0 0 32 32" className="stats-icon ui-icon">
        <use xlinkHref="/assets/sprite.svg#icon-pencil" />
      </svg>
    );
  }
});

exports.BeakerIcon = React.createClass({
  displayName: "Beaker Icon",

  render() {
    return (
      <svg
        className="test-icon ui-icon"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 32 32"
      >
        <use xlinkHref="/assets/sprite.svg#icon-beaker" />
      </svg>
    );
  }
});

exports.PlusIcon = React.createClass({
  displayName: "Plus Icon",
  render() {
    return (
      <svg
        className="dropdown__plus-icon-svg"
        xmlns="http://www.w3.org/2000/svg"
        id="Layer_1"
        data-name="Layer 1"
        viewBox="0 0 31.17 31.17"
      >
        <path d="M15.56 0v31.17M0 15.56h31.17" className="cls-1" />
      </svg>
    );
  }
});

//ABout social Icon

exports.AboutTwitter = React.createClass({
  displayName: "About Twitter",

  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 512 512"
        className="twitter icon__svg icon__svg--team"
      >
        <use xlinkHref="/assets/sprite.svg#twitter" />
      </svg>
    );
  }
});

exports.AboutLinkedIn = React.createClass({
  displayName: "About LinkedIn",

  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="linkedin icon__svg icon__svg--team"
        viewBox="0 0 430.12 411"
      >
        <use xlinkHref="/assets/sprite.svg#linkedin" />
      </svg>
    );
  }
});

exports.AboutAngelCo = React.createClass({
  displayName: "About AngelCo",

  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        className="angel icon__svg icon__svg--team"
        viewBox="0 0 21.9 31.4"
      >
        <use xlinkHref="/assets/sprite.svg#angelco" />
      </svg>
    );
  }
});

exports.DeleteCross = React.createClass({
  displayName: "DeleteCross",
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        id="icon-delete-cross"
        viewBox="0 0 32 32"
        className="DeleteCross"
      >
        <path
          className="path1"
          d="M14.47 15.893l-14.164 14.164c-0.394 0.393-0.394 1.031 0 1.425 0.197 0.197 0.455 0.295 0.712 0.295s0.516-0.098 0.712-0.295l14.271-14.27 14.271 14.27c0.197 0.197 0.454 0.295 0.712 0.295s0.515-0.098 0.712-0.295c0.394-0.393 0.394-1.031 0-1.425l-14.163-14.164 14.173-14.174c0.394-0.393 0.394-1.031 0-1.425s-1.031-0.393-1.424 0l-14.279 14.28-14.281-14.28c-0.393-0.393-1.030-0.393-1.424 0s-0.394 1.031 0 1.425l14.174 14.174z"
        />
      </svg>
    );
  }
});
