/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "Docs",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <h1>Ruby</h1>
        <p>
          You can use fuzzy.ai from your Ruby project using the Open Source
          library.
        </p>
        <section>
          <h2>Installation</h2>
          <p>Add this line to your application's Gemfile:</p>
          <pre>{`\
gem \'fuzzy.ai\'\
`}</pre>
          <p>And then execute:</p>
          <pre>{`\
$ bundle\
`}</pre>
          <p>Or install it yourself as:</p>
          <pre>{`\
$ gem install fuzzy.ai\
`}</pre>
          <p>
            You can download the software at:{" "}
            <a href="https://github.com/fuzzy-ai/fuzzy.ai-ruby">
              https://github.com/fuzzy-ai/fuzzy.ai-ruby
            </a>
          </p>
        </section>
        <section>
          <h2>Usage</h2>
          <p>This gem handles the most basic usage of Fuzzy.ai.</p>
          <pre>{`\
# Require the module

require \'fuzzy.ai\'

# Your API key (get one from https:\x2F\x2Ffuzzy.ai\x2F)

API_KEY = \'YOUR_API_KEY_HERE\'

# Create an account object

acct = FuzzyAi::Account.new API_KEY

# ID of the agent you want to call; get it from https:\x2F\x2Ffuzzy.ai\x2F

AGENT_ID = \'AGENT_ID_HERE\'

# Inputs; map of string or symbol to numbers

inputs = Hash.new
inputs[:input1] = 10
inputs[:input2] = 30

# Ask the agent to evaluate the inputs; returns two values!

outputs, evaluation_id = acct.evaluate AGENT_ID, inputs

# Outputs is all the outputs

puts outputs[\"output1\"]

# An opaque ID for the evaluation

puts evaluation_id

# For feedback, provide a performance metric

performance = Hash.new
performance[:performance1] = 3

fb = acct.feedback evaluation_id, performance\
`}</pre>
        </section>
        <section>
          <h2>FuzzyIo::Account</h2>
          <p>
            Class representing a single account; you can use it to do
            evaluations and give feedback.
          </p>
          <h3>initialize(key)</h3>
          <p>
            Takes a string representing the API key. You can get the key on the
            top of your account page on https://fuzzy.ai/ .
          </p>
          <h3>evaluate(agent_id, inputs)</h3>
          <p>
            Takes a string representing the agent ID, and a hash mapping input
            names (strings or symbols) to numbers.{" "}
          </p>
          <p>
            Response is two values (2!) -- a map of the output names to numbers,
            and a string for the evaluation ID.{" "}
          </p>
          <h3>feedback(evaluation_id, peformance)</h3>
          <p>
            Takes a string for the evaluation ID, and a hash mapping performance
            metrics to numbers. You can provide more than one metric.
          </p>
          <p>
            {" "}
            The metrics will be optimized to decrease toward zero -- so,
            distance from target, etc. If you have a number that will go towards
            infinity, give its inverse (1/x).{" "}
          </p>
        </section>
      </div>
    );
  }
});
