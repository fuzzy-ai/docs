/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "pricing icon",

  // coffeelint: disable=max_line_length
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        id="versatile-cross-icon"
        className="home-svg-icon path"
        viewBox="0 0 501.16 194.65"
      >
        <path
          id="bg-swiss-cross-cut"
          d="M353.35 74.29v59.62H295.2v57.49h-61.61"
          className="cls-1"
        />
        <g id="swiss-cross">
          <path
            id="swiss-cross-shape"
            d="M223.86 119.49h-58.15v-55.5h58.15V6.49h57.49v57.5h58.15v55.5h-58.16v57.49h-57.48v-57.49z"
            className="cls-1"
          />
        </g>
      </svg>
    );
  }
});
