/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");
const { PlusIcon } = require("./icons");

module.exports = React.createClass({
  displayName: "Docs Menu",

  componentDidMount() {
    const docTrig = document.querySelector(".docs-nav-trigger");
    const docSidebar = document.querySelector(".docs-sidebar");

    //toggle menu with button
    const toggleDocsMenu = function() {
      docSidebar.classList.toggle("expand-is-open");
    };
    //add event listener to button
    if (docTrig != null) {
      docTrig.addEventListener("click", toggleDocsMenu);
    }

    //toggle menu when user clicks on a docs link container das cheats yo

    const linkToggleDocsMenu = function() {
      docSidebar.classList.toggle("expand-is-open");
    };

    //add event listerner to container
    return docSidebar != null
      ? docSidebar.addEventListener("click", linkToggleDocsMenu)
      : undefined;
  },

  // coffeelint: disable=max_line_length
  render() {
    return (
      <div>
        <a href="#" className="docs-nav-trigger">
          Docs Navigation{" "}
          <span className="trigger-sub-docs">
            <PlusIcon />
          </span>
        </a>
        <ul className="docs-sidebar compressed-list expand">
          <li className="compressed-list__item docs-sidebar__list">
            <Link
              to="/getting-started"
              activeClassName="active-doc-nav"
              className="docs-sidebar__list-anchor fwb"
            >
              Getting Started
            </Link>{" "}
          </li>
          <li className="compressed-list__item docs-sidebar__list">
            <Link
              to="/sdk"
              className="docs-sidebar__list-anchor fwb"
              activeClassName="active-doc-nav"
            >
              SDKs{" "}
            </Link>
            <ul className="docs-sidebar__ul">
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/sdk/nodejs"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  NodeJS
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/sdk/php"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  PHP
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/sdk/python"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  Python
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/sdk/ruby"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  Ruby
                </Link>{" "}
              </li>
            </ul>
          </li>
          <li className="compressed-list__item docs-sidebar__list">
            <Link
              to="/rest"
              className="docs-sidebar__list-anchor fwb"
              activeClassName="active-doc-nav"
            >
              REST API{" "}
            </Link>
            <ul className="docs-sidebar__ul">
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/rest/about-rest"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  About REST
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/rest/data"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  Data
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/rest/authentication"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  Authentication
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/rest/inference"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  Inference
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/rest/agent"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  Agent lifecycle
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/rest/agent-version"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  Agent version
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/rest/evaluation"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  Evaluation and feedback
                </Link>{" "}
              </li>
              <li className="compressed-list__item docs-sidebar__list">
                <Link
                  to="/rest/api-version"
                  className="docs-sidebar__list-anchor"
                  activeClassName="active-doc-nav"
                >
                  API version
                </Link>{" "}
              </li>
            </ul>
          </li>
        </ul>
      </div>
    );
  }
});
