/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const { Link } = require("react-router");

module.exports = React.createClass({
  displayName: "company logo",

  render() {
    return (
      <div className="cie-logo-wrap">
        <figure className="cie-logo-wrap__inner">
          <svg xmlns="http://www.w3.org/2000/svg" className={this.props.class}>
            <use xlinkHref={`/assets/sprite.svg${this.props.svgId}`} />
          </svg>
        </figure>
      </div>
    );
  }
});
