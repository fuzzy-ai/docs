const { combineReducers } = require('redux');
const { routerReducer } = require('react-router-redux');
const { requestsReducer } = require('redux-requests');

const user = require('./user');

module.exports = combineReducers({
  requests: requestsReducer,
  routing: routerReducer,
  user
});
