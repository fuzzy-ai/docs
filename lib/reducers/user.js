// src/reducers/user.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const _ = require('lodash');

const initialState = {
  inProgress: false,
  authenticated: false,
  account: null,
  error: null,
  needsReConfirm: false
};

module.exports = function(state = initialState, action) {
  const newState = (() => { switch (action != null ? action.type : undefined) {
    case 'USER_RESET':
      return initialState;
    case 'USER_SET':
      return {account: _.assign({}, state.account, action.user)};
    case 'LOGIN_USER':
      return {inProgress: true};
    case 'LOGIN_USER_SUCCESS':
      return {
        inProgress: false,
        authenticated: true,
        error: null,
        account: action.user
      };
    case 'LOGIN_USER_ERROR':
      return {
        inProgress: false,
        authenticated: false,
        error: action.error,
        needsReConfirm: action.needsReConfirm
      };
    case 'LOGOUT_USER':
      return {inProgress: true};
    case 'LOGOUT_USER_SUCCESS':
      return initialState;
    case 'LOGOUT_USER_ERROR':
      return {
        inProgress: false,
        authenticated: true
      };
    case 'REQUEST_INVITE':
      return {inProgress: true};
    case 'REQUEST_INVITE_SUCCESS':
      return {
        inProgress: false,
        error: null
      };
    case 'REQUEST_INVITE_ERROR':
      return {
        inProgress: false,
        error: action.message
      };
    case 'SIGNUP':
      return {inProgress: true};
    case 'SIGNUP_SUCCESS':
      return {
        inProgress: false,
        error: null
      };
    case 'SIGNUP_ERROR':
      return {
        inProgress: false,
        needsReConfirm: action.needsReConfirm,
        error: action.message
      };
    case 'RESEND_CONFIRMATION':
      return {inProgress: true};
    case 'RESEND_CONFIRMATION_SUCCESS':
      return {
        inProgress: false,
        needsReConfirm: false,
        error: action.message
      };
    case 'RESEND_CONFIRMATION_ERROR':
      return {
        inProgress: false,
        needsReConfirm: false,
        error: action.message
      };
    case 'UPDATE_USER':
      return {inProgress: true};
    case 'UPDATE_USER_SUCCESS':
      return {
        inProgress: false,
        authenticated: true,
        error: null,
        account: action.user
      };
    case 'UPDATE_USER_ERROR':
      return {
        inProgress: false,
        message: action.message
      };
    case 'CHANGE_PASSWORD':
      return {inProgress: true};
    case 'CHANGE_PASSWORD_SUCCESS':
      return {
        inProgress: false,
        error: null
      };
    case 'CHANGE_PASSWORD_ERROR':
      return {
        inProgress: false,
        error: action.error
      };
    case 'RESET_REQUEST':
      return {inProgress: true};
    case 'RESET_REQUEST_SUCCESS':
      return {inProgress: false};
    case 'RESET_REQUEST_ERROR':
      return {
        inProgress: false,
        error: action.error
      };
    case 'RESET_PASSWORD':
      return {inProgress: true};
    case 'RESET_PASSWORD_SUCCESS':
      return {
        inProgress: false,
        error: null
      };
    case 'RESET_PASSWORD_ERROR':
      return {
        inProgress: false,
        error: action.error
      };
  } })();

  if (newState != null) {
    return _.assign({}, state, newState);
  } else {
    return state;
  }
};
