/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS103: Rewrite code to no longer use __guard__
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
const React = require("react");
const Helmet = require("react-helmet");
const { Provider } = require("react-redux");
const { createStore } = require("redux");
const { renderToString } = require("react-dom/server");
const { match, RouterContext, createMemoryHistory } = require("react-router");
const { syncHistoryWithStore } = require("react-router-redux");

const configureStore = require("./store");
const configureRoutes = require("./components/routes");

// coffeelint: disable=max_line_length
const renderFullPage = function(content, head, initialState) {
  if (head == null) {
    head = { title: "", meta: "", link: "", script: "" };
  }
  let thirdPartyJS = "";
  let thirdPartyHead = "";
  if (process.env["SMOOCH_TOKEN"]) {
    thirdPartyJS += `\
<script src="https://cdn.smooch.io/smooch.min.js"></script>
<script>Smooch.init({ appToken: '${process.env["SMOOCH_TOKEN"]}' });</script>\
`;
  }

  if (process.env["GA_TOKEN"]) {
    thirdPartyHead += `\
<style>.async-hide { opacity: 0 !important} </style>
<script type="text/javascript">
  (function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
  h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
  (a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
  })(window,document.documentElement,'async-hide','dataLayer',4000,
  {'${process.env["GA_OPTIMIZE_TOKEN"]}':true});
</script>\
`;

    thirdPartyJS += `\
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga')

  ga('create', '${process.env["GA_TOKEN"]}', 'auto');
  ga('require', '${process.env["GA_OPTIMIZE_TOKEN"]}');
  ga('send', 'pageview');
</script>\
`;
  }
  if (process.env["INTERCOM_TOKEN"]) {
    thirdPartyJS += `\
<script type="text/javascript">
  (function(){var w=window;var ic=w.Intercom;if(typeof ic==="function"){ic('reattach_activator');ic('update',intercomSettings);}else{var d=document;var i=function(){i.c(arguments)};i.q=[];i.c=function(args){i.q.push(args)};w.Intercom=i;function l(){var s=d.createElement('script');s.type='text/javascript';s.async=true;
  s.src='https://widget.intercom.io/widget/{app_id}';
  var x=d.getElementsByTagName('script')[0];x.parentNode.insertBefore(s,x);}if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})()

  Intercom('boot', {app_id: '${process.env["INTERCOM_TOKEN"]}'})
</script>\
`;
  }
  if (process.env["TWITTER_PIXEL_ID"]) {
    thirdPartyJS += `\
<script>
  !function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
  },s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
  a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
  // Insert Twitter Pixel ID and Standard Event data below
  twq('init','${process.env["TWITTER_PIXEL_ID"]}');
  twq('track', 'PageView');
</script>\
`;
  }
  if (process.env["FACEBOOK_PIXEL_ID"]) {
    thirdPartyJS += `\
<script>
  !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  document,'script','https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '${process.env["FACEBOOK_PIXEL_ID"]}', {
  em: '${__guard__(
    initialState.user != null ? initialState.user.account : undefined,
    x => x.email
  )}'
  });
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=${
      process.env["FACEBOOK_PIXEL_ID"]
    }&ev=PageView&noscript=1"
/></noscript>\
`;
  }
  return `\
<!DOCTYPE html>
  <html>
  <head>
      ${head.title}
      ${head.meta}
      ${thirdPartyHead}
      ${head.link}
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/picturefill/3.0.2/picturefill.min.js"></script>

      <script type="text/javascript">
        document.createElement( "picture" );
      </script>
  </head>
  <body>
  <div id="app">${content}</div>
  <noscript>
    <div class="nojs-wrapper">
      <div class="nojs message">
        <h2 class="upper h4 fwb">Javascript is required</h2>
        <p>Oops, it looks like JavaScript is disabled in your browser. Please enabled JavaScript to use Fuzzy.ai. If you have any questions <a href="mailto:support@fuzzy.ai">please contact our support team</a>.</p>
      </div>
    </div>
  </noscript>
  ${head.script}
  <script>
    window.__INITIAL_STATE__ = ${JSON.stringify(initialState)};
  </script>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  <script type="text/javascript" charset="utf-8" src="/dist/bundle.js"></script>
  <script type="text/javascript">
    Stripe.setPublishableKey('${process.env["WEB_STRIPE_PUBLIC_KEY"]}');
  </script>
  <script src="https://use.typekit.net/dpp3xnk.js"></script>
  <script>try{Typekit.load({ async: true });}catch(e){}</script>
  ${thirdPartyJS}
  </body>
  </html>\
`;
};

const fetchAll = (store, routerState) =>
  routerState.components.map(function(componentClass) {
    if (
      componentClass.WrappedComponent != null
        ? componentClass.WrappedComponent.fetchData
        : undefined
    ) {
      return componentClass.fetchData(store.dispatch, routerState.params);
    }
  });

module.exports = function(req, res, next) {
  // Don't match components if we've already sent data
  if (res.headersSent) {
    return;
  }

  let initialState = {
    user: {
      authenticated:
        (req.session != null ? req.session.user : undefined) != null,
      account: req.session != null ? req.session.user : undefined
    }
  };

  if (req.session.tutorial != null) {
    initialState.tutorial = req.session.tutorial;
  }

  const memoryHistory = createMemoryHistory(req.url);
  const store = configureStore(memoryHistory, initialState);
  const history = syncHistoryWithStore(memoryHistory, store);
  const routes = configureRoutes(store);

  return match({ history, routes, location: req.url }, function(
    err,
    redirectLocation,
    routerState
  ) {
    if (err) {
      return next(err);
    } else if (redirectLocation) {
      return res.redirect(
        302,
        redirectLocation.pathname + redirectLocation.search
      );
    } else if (routerState) {
      return Promise.all(fetchAll(store, routerState))
        .then(function() {
          const content = renderToString(
            <Provider store={store}>
              <RouterContext {...Object.assign({}, routerState)} />
            </Provider>
          );
          const head = Helmet.rewind();
          initialState = store.getState();

          const status = routerState.routes.reduce(
            (prev, cur) => cur.status || prev,
            200
          );
          res.set("Content-Type", "text/html");
          res.status(status).write(renderFullPage(content, head, initialState));
          return res.end();
        })
        .catch(err => next(err));
    } else {
      return next();
    }
  });
};

function __guard__(value, transform) {
  return typeof value !== "undefined" && value !== null
    ? transform(value)
    : undefined;
}
