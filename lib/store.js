const { createStore, compose, applyMiddleware } = require('redux');
const { routerMiddleware } = require('react-router-redux');
const { createRequestMiddleware } = require('redux-requests');
const thunkMiddleware = require('redux-thunk').default;

const reducers = require('./reducers');

module.exports = (history, initialState) =>
  createStore(
    reducers,
    initialState,
    compose(
      applyMiddleware(
        thunkMiddleware,
        createRequestMiddleware(),
        routerMiddleware(history)
      ),
      ((typeof window !== 'undefined' && window !== null ? window.devToolsExtension : undefined) != null) ?
        window.devToolsExtension()
      :
        f => f
    )
  )
;
