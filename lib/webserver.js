// webserver.coffee
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const http = require('http');
const https = require('https');
let url = require('url');
const path = require('path');
const assert = require('assert');
const os = require('os');

const _ = require('lodash');
const async = require('async');
const express = require('express');
const bodyParser = require('body-parser');
const favicon = require('serve-favicon');
const db = require('databank');
const Logger = require('bunyan');
const uuid = require('uuid');
const session = require('express-session');
const validator = require('validator');
const DatabankStore = require('connect-databank')(session);
const request = require('request');

const App = require('./server');

const HTTPError = require('./httperror');
const version = require('./version');

const { Databank } = db;
const { DatabankObject } = db;

const defaults = {
  port: 80,
  hostname: "localhost",
  address: "0.0.0.0",
  key: null,
  cert: null,
  driver: "memory",
  params: {},
  logFile: null,
  logLevel: "info",
  sessionSecret: "casey perle gauze cy file",
  caCert: null,
  cleanup: 3600000,
  apiClientCacheSize: 50,
  sessionMaxAge: 30 * 24 * 60 * 60 * 1000, // 30 days
  sessionSecure: false
};

const isJSONType = function(type) {
  const expected = "application/json";
  return type.substr(0, expected.length) === expected;
};

class WebServer {

  getName() {
    return "docs";
  }

  constructor(config) {

    const webserver = this;

    const setupLogger = function(cfg) {
      const logParams = {
        serializers: {
          req: Logger.stdSerializers.req,
          res: Logger.stdSerializers.res,
          err: Logger.stdSerializers.err
        },
        level: cfg.logLevel
      };

      if (cfg.logFile) {
        logParams.streams = [{path: cfg.logFile}];
      } else {
        logParams.streams = [{stream: process.stdout}];
      }

      logParams.name = webserver.getName();

      const log = new Logger(logParams);

      log.info(`Log level = ${logParams.level}`);

      if (logParams.streams[0].path != null) {
        log.info(`Logging to ${logParams.streams[0].path}`);
      } else if (logParams.streams[0].stream != null) {
        log.info("Logging to standard output");
      }

      return log;
    };

    const setupExpress = function(cfg, log) {

      const sendToSlack = function(message, icon, attachment, callback) {
        if ((callback == null)) {
          callback = attachment;
          attachment = null;
        }
        if ((cfg.slackHook == null)) {
          return callback(null);
        } else {
          const options = {
            url: cfg.slackHook,
            headers: {
              "Content-Type": "application/json"
            },
            json: {
              text: message,
              username: "home",
              icon_emoji: icon
            }
          };
          // Attachment text
          if (attachment != null) {
            options.json.attachments = [{text: attachment}];
          }
          return request.post(options, function(err, response, body) {
            if (err) {
              return callback(err);
            } else {
              return callback(null);
            }
          });
        }
      };

      const extendedMessage = function(err) {
        const ct = err.headers['content-type'];
        if ((ct != null) && isJSONType(ct)) {
          return JSON.parse(err.body).message;
        } else {
          return null;
        }
      };

      const requestLogger = function(req, res, next) {
        const start = Date.now();
        req.id = uuid.v4();
        const weblog = req.app.log.child({
          "req_id": req.id,
          "url": req.originalUrl,
          method: req.method,
          component: "web"
        });
        const { end } = res;
        req.log = weblog;
        res.end = function(chunk, encoding) {
          res.end = end;
          res.end(chunk, encoding);
          const rec = {req, res, elapsed: Date.now() - start};
          weblog.info(rec, "Completed request.");
          return next();
        };
        return next();
      };

      const sessionUser = function(req, res, next) {
        if (req.session != null ? req.session.user : undefined) {
          const uprops = _.omit(req.session != null ? req.session.user : undefined, ["password", "passwordHash"]);
          res.locals.user = uprops;
          req.user = uprops;
        } else {
          res.locals.user = null;
          req.user = null;
        }
        return next();
      };

      const exp = express();
      exp.config = cfg;
      exp.log = log;

      // Hide the "X-Powered-By" header Express uses

      exp.disable('x-powered-by');

      // Redirect if we're HTTPS and getting hit by HTTP

      if (cfg.redirectToHttps) {
        exp.use(function(req, res, next) {
          if (req.headers['x-forwarded-proto'] === "http") {
            return res.redirect(301, `https://${req.hostname}${req.url}`);
          } else {
            return next();
          }
        });
      }

      // Add our own "Server" header

      exp.use(function(req, res, next) {
        res.set('Server', `docs.fuzzy.ai/${version}`);
        return next();
      });

      // Short-circuit if it's a Google Health Check
      // No session or other setup

      exp.use(function(req, res, next) {
        if (req.headers["user-agent"] != null ? req.headers["user-agent"].match("^GoogleHC") : undefined) {
          res.set("Content-Type", "text/plain");
          return res.send("OK");
        } else {
          return next();
        }
      });

      // Develpment mode tweaks
      if (process.env.NODE_ENV === 'development') {
        const webpack = require('webpack');
        const webpackConfig = require('../webpack.config');
        webpackConfig.entry.unshift('webpack-hot-middleware/client?reload=true');
        webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
        const compiler = webpack(webpackConfig);

        exp.use(require('webpack-dev-middleware')(compiler, {
          noInfo: true,
          publicPath: webpackConfig.output.publicPath
        })
        );
        exp.use(require('webpack-hot-middleware')(compiler));
      }

      exp.use(express.static(path.join(__dirname, '..', 'public')));

      // XXX: this should get passed down but YOLO

      const store = new DatabankStore(webserver.db, exp.log, cfg.cleanup);

      exp.use(session({
        secret: cfg.sessionSecret,
        store,
        resave: true,
        saveUninitialized: true,
        cookie: {
          maxAge: cfg.sessionMaxAge,
          secure: cfg.sessionSecure
        }
      })
      );

      exp.use(sessionUser);
      exp.use(bodyParser.urlencoded({extended: true}));
      exp.use(bodyParser.json());
      const fiPath = path.join(__dirname, '..', 'public', 'images', 'favicon.ico');
      exp.use(favicon(fiPath));
      exp.use(requestLogger);

      exp.get('/version', (req, res, next) => res.json({name: webserver.getName(), version}));

      // React Router
      exp.get('*', (req, res, next) => App(req, res, next));

      // Error handler
      exp.use(function(err, req, res, next) {

        config = __guard__(req != null ? req.app : undefined, x => x.config);

        if (err.statusCode) {
          res.statusCode = err.statusCode;
        } else {
          switch (err.name) {
            case "NoSuchThingError":
              res.statusCode = 404;
              break;
            case "URIError":
              res.statusCode = 400;
              break;
            case "SyntaxError":
              res.statusCode = 400;
              break;
            default:
              res.statusCode = 500;
          }
        }

        if (req.log) {
          req.log.error({err}, "Error");
        }

        const userId = (req.user != null ? req.user.email : undefined) || "(unauthenticated)";
        const { method } = req;
        url = req.originalUrl;

        const reqInfo = `${method} ${url} by ${userId}`;

        // Post 5xx errors to Slack

        if ((res.statusCode >= 500) && (res.statusCode < 600)) {
          const hostname = os.hostname();
          const messageText = `${hostname} ${err.name}: ${err.message}\n${reqInfo}`;
          sendToSlack(messageText, ":bomb:", function(err) {
            if (err) {
              return req.log.error({err}, "Error posting to Slack");
            }
          });
        }

        if (!res.headerSent) {
          // XXX: route through error component
          return res.json({status: 'error', message: err.message});
        }});

      return exp;
    };

    const startNetwork = callback => {

      if (this.config.key) {
        this.log.info("Creating server with HTTPS");
        const options = {
          key: this.config.key,
          cert: this.config.cert
        };
        this.srv = https.createServer(options, this.express);
      } else {
        this.log.info("Creating server with HTTP");
        this.srv = http.createServer(this.express);
      }

      this.srv.once('error', err => callback(err));
      this.srv.once('listening', () => callback(null));
      const address = this.config.address || this.config.hostname;
      this.log.info(`Listening on ${address}:${this.config.port}`);
      return this.srv.listen(this.config.port, address);
    };

    const stopNetwork = callback => {

      if ((this.srv == null)) {
        return callback(null);
      } else {
        this.srv.once('close', () => callback(null));
        this.srv.once('error', err => callback(err));
        return this.srv.close();
      }
    };

    const startDatabase = callback => {
      if (this.db != null) {
        return this.db.connect(this.config.params, callback);
      } else {
        return callback(new Error("db property not set for webserver"));
      }
    };

    const stopDatabase = callback => {
      if (this.db != null) {
        return this.db.disconnect(callback);
      } else {
        return callback(null);
      }
    };

    this.config = _.defaults(config, defaults);

    if (this.config.key && (this.config.port === defaults.port)) {
      this.config.port = 443;
    }

    // Databank is not yet connected!

    this.db = Databank.get(this.config.driver, this.config.params);

    this.log = setupLogger(this.config);
    this.express = setupExpress(this.config, this.log);

    this.start = callback => {

      return async.waterfall([
        callback => startDatabase(callback),
        callback => startNetwork(callback)
      ], callback);
    };

    this.stop = callback =>

      async.waterfall([
        callback => stopNetwork(callback),
        callback => stopDatabase(callback)
      ], callback)
    ;
  }
}

module.exports = WebServer;

function __guard__(value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}