// fetch-error.coffee
// Error class for when a fetch() call goes wrong
// Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved.

class FetchError extends Error {
  constructor(response, json) {
    this.status = response.status;
    this.message = json != null ? json.message : undefined;
  }
}

module.exports = FetchError;
