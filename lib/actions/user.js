require('es6-promise');
const fetch = require('isomorphic-fetch');
const { push } = require('react-router-redux');

const { checkResponse } = require('.');
const FetchError = require('../fetch-error');

exports.reset = () => ({type: 'USER_RESET'});

const beginLogin = () => ({ type: 'LOGIN_USER' });

const loginSuccess = user =>
  ({
    type: 'LOGIN_USER_SUCCESS',
    user
  })
;

const loginError = (err, needsReConfirm) =>
  ({
    type: 'LOGIN_USER_ERROR',
    error: err,
    needsReConfirm
  })
;

exports.logIn = data =>
  function(dispatch) {
    dispatch(beginLogin());

    return fetch('/login', {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(function(response) {
      if ((response.status >= 200) && (response.status < 300)) {
        return response.json();
      } else {
        return response.json()
          .then(function(json) {
            throw new FetchError(response, json);}).catch(function(err) {
            throw new FetchError(response, err);
        });
      }}).then(function(data) {
      if (data.status === 'OK') {
        dispatch(loginSuccess(data.user));
        return dispatch(push(data.url));
      } else {
        let needsReConfirm = false;
        if (data.message === "This account needs to be confirmed.") {
          needsReConfirm = true;
        }
        return dispatch(loginError(data, needsReConfirm));
      }}).catch(err => dispatch(loginError(err, false)));
  }
;

const beginRequest = () => ({ type: 'REQUEST_INVITE' });

const requestSuccess = message => ({type: 'REQUEST_INVITE_SUCCESS'});

const requestError = message =>
  ({
    type: 'REQUEST_INVITE_ERROR',
    message
  })
;

exports.requestInvite = data =>
  function(dispatch) {
    dispatch(beginRequest());

    return fetch('/request', {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }
    ).then(function(response) {
      if (response.status === 200) {
        dispatch(requestSuccess());
        dispatch(push('/request-complete'));
        return;
      } else {
        return response.json();
      }
    }).then(function(data) {
      if (data) {
        return dispatch(requestError(data.message));
      }
    });
  }
;

const beginSignup = () => ({ type: 'SIGNUP'});

const signupSuccess = () => ({ type: 'SIGNUP_SUCESS' });

const signupError = (message, needsReConfirm) =>
  ({
    type: 'SIGNUP_ERROR',
    message,
    needsReConfirm
  })
;

exports.signup = data =>
  function(dispatch) {
    dispatch(beginSignup());

    return fetch('/signup', {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }
    ).then(function(response) {
      if (response.status === 200) {
        __guardMethod__(window, 'Intercom', o => o.Intercom('trackEvent', 'user-signup',
        __guardMethod__(window, 'ga', o1 => o1.ga('send', {
          hitType: 'event',
          eventCategory: 'User',
          eventAction: 'Sign up'
        }
        ))
        ));
        dispatch(signupSuccess());
        dispatch(push('/signup-complete'));
        return;
      } else {
        return response.json();
      }
    }).then(function(data) {
      if (data) {
        let needsReConfirm = false;
        if (data.message.match(/.*(confirmed).*/)) {
          needsReConfirm = true;
        }
        return dispatch(signupError(data.message, needsReConfirm));
      }
    });
  }
;

exports.setUser = user =>
  ({
    type: 'USER_SET',
    user
  })
;

const beginLogout = () => ({ type: 'LOGOUT_USER'});

const logoutSuccess = () => ({ type: 'LOGOUT_USER_SUCCESS'});

const logoutError = () => ({ type: 'LOGOUT_USER_ERROR'});

exports.logOut = () =>
  function(dispatch) {
    dispatch(beginLogout());

    return fetch('/logout', {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(function(response) {
      if (response.status === 200) {
        dispatch(logoutSuccess());
        return dispatch(push('/login'));
      } else {
        return dispatch(logoutError());
      }
    });
  }
;

const beginResendConfirm = () => ({type: 'RESEND_CONFIRMATION'});

const resendConfirmationSuccess = message =>
  ({
    type: 'RESEND_CONFIRMATION_SUCCESS',
    message
  })
;

const resendConfirmationError = message =>
  ({
    type: 'RESEND_CONFIRMATION_ERROR',
    message
  })
;

exports.resendConfirmation = data =>
  function(dispatch) {
    dispatch(beginResendConfirm());

    return fetch('/resend-confirmation', {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }
    ).then(response => response.json()).then(function(json) {
      if (json.status === 'OK') {
        json.message = "Check your email to confirm your account.";
        return dispatch(resendConfirmationSuccess(json.message));
      } else {
        return dispatch(resendConfirmationError(json.message));
      }
    });
  }
;

const beginUpdateUser = () => ({type: 'UPDATE_USER'});

const updateUserSuccess = user =>
  ({
    type: 'UPDATE_USER_SUCCESS',
    user
  })
;

const updateUserError = message =>
  ({
    type: 'UPDATE_USER_SUCCESS',
    message
  })
;

exports.updateUser = data =>
  function(dispatch) {
    dispatch(beginUpdateUser());

    return fetch('/data/user', {
      method: 'put',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }
    ).then(response => response.json()).then(function(json) {
      if (json.status === 'OK') {
        return dispatch(updateUserSuccess(json.user));
      } else {
        return dispatch(updateUserError(json.message));
      }
    });
  }
;

const beginChangePassword = () => ({type: 'CHANGE_PASSWORD'});

const changePasswordSuccess = user =>
  ({
    type: 'CHANGE_PASSWORD_SUCCESS',
    user
  })
;

const changePasswordError = err =>
  ({
    type: 'CHANGE_PASSWORD_ERROR',
    error: err
  })
;

exports.changePassword = data =>
  function(dispatch) {
    dispatch(beginChangePassword());
    return fetch('/data/change-password', {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(response => checkResponse(response, dispatch))
    .then(json => dispatch(changePasswordSuccess(json.user))).catch(err => dispatch(changePasswordError(err)));
  }
;

const beginResetRequest = () => ({type: 'RESET_REQUEST'});

const resetRequestSuccess = () => ({type: 'RESET_REQUEST_SUCCESS'});

const resetRequestError = err =>
  ({
    type: 'RESET_REQUEST_ERROR',
    error: err
  })
;

exports.resetRequest = data =>
  function(dispatch) {
    dispatch(beginResetRequest());

    return fetch('/reset', {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(response => checkResponse(response, dispatch))
    .then(function(json) {
      dispatch(resetRequestSuccess());
      return dispatch(push('/reset-requested'));}).catch(err => dispatch(resetPasswordError(err)));
  }
;

const beginResetPassword = () => ({type: 'RESET_PASSWORD'});

const resetPasswordSuccess = () => ({type: 'RESET_PASSWORD_SUCCESS'});

var resetPasswordError = err =>
  ({
    type: 'RESET_PASSWORD_ERROR',
    error: err
  })
;

exports.resetPassword = data =>
  function(dispatch) {
    dispatch(beginResetPassword());

    return fetch('/change-password', {
      method: 'post',
      credentials: 'same-origin',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then(response => response.json())
    .then(function(json) {
      if (json.status === 'OK') {
        dispatch(resetPasswordSuccess());
        return dispatch(push('/password-changed'));
      } else {
        return dispatch(resetPasswordError(json));
      }}).catch(err => dispatch(resetPasswordError(json)));
  }
;

function __guardMethod__(obj, methodName, transform) {
  if (typeof obj !== 'undefined' && obj !== null && typeof obj[methodName] === 'function') {
    return transform(obj, methodName);
  } else {
    return undefined;
  }
}