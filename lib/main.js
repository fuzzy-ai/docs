// main.coffee
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

require('dns-notfound-what');

const WebServer = require('./webserver');

const envBool = function(str) {
  let needle;
  return (needle = str != null ? str.toLowerCase() : undefined, ["true", "yes", "1", "on"].includes(needle));
};

// coffeelint: disable=max_line_length
const config = {
  port: process.env['WEB_PORT'],
  hostname: process.env['WEB_HOSTNAME'],
  address: process.env['WEB_ADDRESS'],
  key: process.env['WEB_KEY'],
  cert: process.env['WEB_CERT'],
  sessionSecret: process.env['WEB_SESSION_SECRET'],
  logLevel: process.env['WEB_LOG_LEVEL'],
  logFile: process.env['WEB_LOG_FILE'],
  caCert: process.env['WEB_CA_CERT'],
  driver: process.env['DRIVER'],
  params: (process.env['PARAMS'] != null) ? JSON.parse(process.env['PARAMS']) : {},
  cleanup: (process.env['CLEANUP'] != null) ? parseInt(process.env['CLEANUP'], 10) : null,
  slackHook: process.env['SLACK_HOOK'],
  redirectToHttps: (process.env['REDIRECT_TO_HTTPS'] != null) ? envBool(process.env['REDIRECT_TO_HTTPS']) : false,
  sessionMaxAge: (process.env['WEB_SESSION_MAX_AGE'] != null) ? parseInt(process.env['WEB_SESSION_MAX_AGE'], 10) : undefined,
  sessionSecure: (process.env['WEB_SESSION_SECURE'] != null) ? envBool(process.env['WEB_SESSION_SECURE']) : false
};

const server = new WebServer(config);

// No errors on too many listeners

process.setMaxListeners(0);

const shutdown = function() {
  console.log("Shutting down...");
  return server.stop(function(err) {
    if (err) {
      console.error(err);
      return process.exit(-1);
    } else {
      console.log("Done.");
      return process.exit(0);
    }
  });
};

process.on('SIGTERM', shutdown);
process.on('SIGINT', shutdown);

// XXX: send a message to Slack for uncaught exceptions

process.on('uncaughtException', function(err) {
  console.log("******************** UNCAUGHT EXCEPTION ********************");
  console.error(err);
  if (err.stack) {
    console.dir(err.stack.split("\n"));
  }
  return process.exit(127);
});

server.start(function(err) {
  if (err) {
    return console.error(err);
  } else {
    return console.log(`Server listening on ${server.config.port} for ${server.config.address || server.config.hostname}`);
  }
});
