/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS103: Rewrite code to no longer use __guard__
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
require("babel-polyfill");
const moment = require("moment");
const React = require("react");
const { render } = require("react-dom");

const { Provider } = require("react-redux");
const {
  Router,
  browserHistory,
  applyRouterMiddleware
} = require("react-router");
const { syncHistoryWithStore } = require("react-router-redux");

const { useScroll } = require("react-router-scroll");

const configureRoutes = require("./components/routes");
const configureStore = require("./store");

const initialState = window.__INITIAL_STATE__;

const store = configureStore(browserHistory, initialState);
const history = syncHistoryWithStore(browserHistory, store);
const routes = configureRoutes(store);

// wire in analytics
history.listen(function(location) {
  const state = store.getState();
  const user = state.user.account;
  if (user) {
    __guardMethod__(window, "Intercom", o =>
      o.Intercom("update", {
        email: user.email,
        user_id: user.id,
        created_at: moment(user.createdAt).unix(),
        plan: user.plan
      })
    );
  } else {
    __guardMethod__(window, "Intercom", o1 => o1.Intercom("update"));
  }
  __guardMethod__(window, "ga", o2 => o2.ga("send", "pageview"));
  __guardMethod__(window, "twq", o3 => o3.twq("track", "PageView"));
  return __guardMethod__(window, "fbq", o4 => o4.fbq("track", "PageView"));
});

require("../public/assets/scss/style.scss");

render(
  <Provider store={store}>
    <Router
      history={history}
      routes={routes}
      render={applyRouterMiddleware(useScroll())}
    />
  </Provider>,
  document.getElementById("app")
);

function __guardMethod__(obj, methodName, transform) {
  if (
    typeof obj !== "undefined" &&
    obj !== null &&
    typeof obj[methodName] === "function"
  ) {
    return transform(obj, methodName);
  } else {
    return undefined;
  }
}
