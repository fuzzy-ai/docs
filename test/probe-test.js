// home-links-test.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury');
const { assert } = vows;
const Browser = require('zombie');
const debug = require('debug')('docs:probe-test');

const env = require('./env');

Browser.localhost('localhost', env['WEB_PORT']);

const { apiBatch } = require('./apibatch');

const testEndpoints = function(endpoints) {
  const obj = {};
  endpoints.forEach(function(endpoint) {
    return obj[`And we check ${endpoint}`] = {
      topic() {
        const br = new Browser();
        const cb = this.callback;
        br.fetch(`http://localhost:${env['WEB_PORT']}${endpoint}`)
          .then(function(response) {
            debug(response);
            if (response.status === 200) {
              return response.json();
            } else {
              throw new Error(`Unexpected response code ${response.status}`);
            }}).then(json => {
            return cb(null, json);
            }).catch(err => {
            return cb(err, null);
        });
        return undefined;
      },
      "it works"(err, json) {
        debug(err);
        debug(json);
        assert.ifError(err);
        return assert.isObject(json);
      }
    };
  });
  return obj;
};

vows
  .describe("Probe endpoints")
  .addBatch(apiBatch(testEndpoints([
      "/liveness.json",
      "/readiness.json",
      "/version"
    ])))
  .export(module);
