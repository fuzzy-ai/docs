// webserver-test.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const util = require('util');

const vows = require('perjury');
const { assert } = vows;
const request = require('request');

process.on('uncaughtException', err => console.error(err));

vows
  .describe('webserver')
  .addBatch({
    'When we load the module': {
      topic() {
        const { callback } = this;
        try {
          const WebServer = require('../lib/webserver');
          callback(null, WebServer);
        } catch (err) {
          callback(err);
        }
        return undefined;
      },
      'it works'(err, WebServer) {
        return assert.ifError(err);
      },
      'it is a class'(err, WebServer) {
        assert.ifError(err);
        return assert.isFunction(WebServer);
      },
      'and we instantiate a WebServer': {
        topic(WebServer) {
          const { callback } = this;
          try {
            const config = {
              port: "2342",
              hostname: "localhost",
              driver: "memory",
              logfile: "/dev/null"
            };
            const server = new WebServer(config);
            callback(null, server);
          } catch (err) {
            callback(err);
          }
          return undefined;
        },
        'it works'(err, server) {
          return assert.ifError(err);
        },
        'it is an object'(err, server) {
          assert.ifError(err);
          return assert.isObject(server);
        },
        'it has a start() method'(err, server) {
          assert.ifError(err);
          assert.isObject(server);
          return assert.isFunction(server.start);
        },
        'it has a stop() method'(err, server) {
          assert.ifError(err);
          assert.isObject(server);
          return assert.isFunction(server.stop);
        },
        'and we start the server': {
          topic(server) {
            const { callback } = this;
            server.start(function(err) {
              if (err) {
                return callback(err);
              } else {
                return callback(null);
              }
            });
            return undefined;
          },
          'it works'(err) {
            return assert.ifError(err);
          },
          'and we request the version': {
            topic() {
              const { callback } = this;
              const url = 'http://localhost:2342/version';
              request.get(url, function(err, response, body) {
                if (err) {
                  return callback(err);
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`));
                } else {
                  body = JSON.parse(body);
                  return callback(null, body);
                }
              });
              return undefined;
            },
            'it works'(err, version) {
              return assert.ifError(err);
            },
            'it looks correct'(err, version) {
              assert.ifError(err);
              assert.include(version, "version");
              return assert.include(version, "name");
            },
            'and we stop the server': {
              topic(version, server) {
                const { callback } = this;
                server.stop(function(err) {
                  if (err) {
                    return callback(err);
                  } else {
                    return callback(null);
                  }
                });
                return undefined;
              },
              'it works'(err) {
                return assert.ifError(err);
              },
              'and we request the version': {
                topic() {
                  const { callback } = this;
                  const url = 'http://localhost:2342/version';
                  request.get(url, function(err, response, body) {
                    if (err) {
                      return callback(null);
                    } else {
                      return callback(new Error("Unexpected success after server stop"));
                    }
                  });
                  return undefined;
                },
                'it fails correctly'(err) {
                  return assert.ifError(err);
                }
              }
            }
          }
        }
      }
    }}).export(module);
