// integration-docs-test.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury');
const { assert } = vows;

const { browser, apiBatch } = require('./apibatch');

vows
  .describe("GET /*")
  .addBatch(apiBatch({
    'and we visit /': {
      topic() {
        browser.visit('/', this.callback);
        return undefined;
      },
      'it works'() {
        return browser.assert.success();
      },
      'and we visit /getting-started': {
        topic() {
          browser.visit('/getting-started', this.callback);
          return undefined;
        },
        'it works'() {
          return browser.assert.success();
        }
      },
      'and we visit /sdk/nodejs': {
        topic() {
          browser.visit('/sdk/nodejs', this.callback);
          return undefined;
        },
        'it works'() {
          return browser.assert.success();
        }
      },
      'and we visit /sdk/python': {
        topic() {
          browser.visit('/sdk/python', this.callback);
          return undefined;
        },
        'it works'() {
          return browser.assert.success();
        }
      },
      'and we visit /sdk/ruby': {
        topic() {
          browser.visit('/sdk/ruby', this.callback);
          return undefined;
        },
        'it works'() {
          return browser.assert.success();
        }
      },
      'and we visit /sdk/php': {
        topic() {
          browser.visit('/sdk/php', this.callback);
          return undefined;
        },
        'it works'() {
          return browser.assert.success();
        }
      },
      'and we visit /rest': {
        topic() {
          browser.visit('/rest', this.callback);
          return undefined;
        },
        'it works'() {
          return browser.assert.success();
        },
        'and we visit /rest/about-rest': {
          topic() {
            browser.visit('/rest/about-rest', this.callback);
            return undefined;
          },
          'it works'() {
            return browser.assert.success();
          }
        },
        'and we visit /rest/data': {
          topic() {
            browser.visit('/rest/data', this.callback);
            return undefined;
          },
          'it works'() {
            return browser.assert.success();
          }
        },
        'and we visit /rest/authentication': {
          topic() {
            browser.visit('/rest/authentication', this.callback);
            return undefined;
          },
          'it works'() {
            return browser.assert.success();
          }
        },
        'and we visit /rest/inference': {
          topic() {
            browser.visit('/rest/inference', this.callback);
            return undefined;
          },
          'it works'() {
            return browser.assert.success();
          }
        },
        'and we visit /rest/agent': {
          topic() {
            browser.visit('/rest/agent', this.callback);
            return undefined;
          },
          'it works'() {
            return browser.assert.success();
          }
        },
        'and we visit /rest/agent-version': {
          topic() {
            browser.visit('/rest/agent-version', this.callback);
            return undefined;
          },
          'it works'() {
            return browser.assert.success();
          }
        },
        'and we visit /rest/evaluation': {
          topic() {
            browser.visit('/rest/evaluation', this.callback);
            return undefined;
          },
          'it works'() {
            return browser.assert.success();
          }
        },
        'and we visit /rest/api-version': {
          topic() {
            browser.visit('/rest/api-version', this.callback);
            return undefined;
          },
          'it works'() {
            return browser.assert.success();
          }
        }
      }
    }
  })).export(module);
