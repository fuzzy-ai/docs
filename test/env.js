module.exports = {
  NODE_ENV: 'production',
  SILENT: 'true',
  WEB_HOSTNAME: "localhost",
  WEB_PORT: 4321,
  WEB_AUTH_SERVER: process.env.WEB_AUTH_SERVER || "http://localhost:8003",
  WEB_AUTH_KEY: 'aDnwaMB2u3k',
  DRIVER: 'memory',
  PARAMS: '{}',
  WEB_LOG_FILE: "/dev/null"
};
