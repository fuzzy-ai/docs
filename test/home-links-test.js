// home-links-test.coffee
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury');
const { assert } = vows;
const Browser = require('zombie');

const env = require('./env');

Browser.localhost('localhost', env['WEB_PORT']);

const { apiBatch } = require('./apibatch');

const homeLinks = [
  "/",
  "/how-it-works",
  "/pricing",
  "/about",
  "/tos",
  "/privacy"
];

vows
  .describe("Home links")
  .addBatch(apiBatch({
    "and we visit /": {
      topic() {
        const browser = new Browser();
        browser.visit(`http://localhost:${env['WEB_PORT']}/`, err => {
          return this.callback(err, browser);
        });
        return undefined;
      },
      'it works'(err, browser) {
        assert.ifError(err);
        browser.assert.success();
        return (() => {
          const result = [];
          for (let rel of homeLinks) {
            const sel = `a[href='https://fuzzy.ai${rel}']`;
            result.push(browser.assert.elements(sel, {atLeast: 1}));
          }
          return result;
        })();
      }
    }
  })).export(module);
