// apibatch.coffee
// Copyright 2017 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury');
const { assert } = vows;
const child_process = require('child_process');
const _ = require('lodash');
const path = require('path');
const Browser = require('zombie');

const env = require('./env');

Browser.localhost('localhost', env['WEB_PORT']);

const browser = new Browser();
browser.on('error', error => console.error(error.stack));

const base = {
  'When we run a webserver': {
    topic() {
      const { callback } = this;
      const modulePath = path.join(__dirname, "..", "lib", "main.js");
      const child = child_process.fork(modulePath, [], {env, silent: true});

      child.once("error", err => callback(err));

      child.stderr.on("data", function(data) {
        const str = data.toString('utf8');
        return process.stderr.write(`SERVER ERROR: ${str}\n`);
      });

      child.stdout.on("data", function(data) {
        const str = data.toString('utf8');
        if (str.match("Server listening")) {
          return callback(null, child);
        } else if (env.SILENT !== "true") {
          return process.stdout.write(`SERVER: ${str}\n`);
        }
      });
      return undefined;
    },
    'it works'(err, child) {
      assert.ifError(err);
      return assert.isObject(child);
    },
    'teardown'(child) {
      return child.kill();
    }
  }
};

const login = {
  'and we visit /login': {
    topic() {
      browser.visit('/login', this.callback);
      return undefined;
    },
    'it works'() {
      return browser.assert.success();
    },
    'and we login': {
      topic() {
        browser.fill('email', 'test@example.com')
          .fill('password', 'testing123')
          .pressButton('Login', this.callback);
        return undefined;
      },
      'it works'() {
        return browser.assert.success();
      }
    }
  }
};

exports.browser = browser;

exports.apiBatch = function(rest) {
  const batch = _.cloneDeep(base);
  const a = 'When we run a webserver';
  _.assign(batch[a], rest);
  return batch;
};

exports.loginBatch = function(rest) {
  const a = 'When we run a webserver';
  const b = 'and we visit /login';
  const c = 'and we login';
  const batch = _.cloneDeep(base);
  const l = _.cloneDeep(login);
  _.assign(batch[a], l);
  _.assign(batch[a][b][c], rest);
  return batch;
};
