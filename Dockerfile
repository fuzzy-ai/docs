FROM node:9-alpine

RUN apk add --no-cache curl

WORKDIR /opt/docs
COPY . /opt/docs

RUN npm install
RUN npm build

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/docs/bin/dumb-init", "--"]
CMD ["npm", "start"]
