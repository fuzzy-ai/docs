var startTour = function(agentID) {
  var steps = [
    {
      path: "/",
      element: "#agent-social-relevance",
      title: "Welcome to your dashboard!",
      content: "This is the list of agents in your account."
    },
    {
      path: "/agent/"+agentID,
      element: "#inputs",
      title: "Social Relevance agent",
      content: "The first thing you’ll see are the usage stats of your agent."
    },
    {
      path: "/agent/"+agentID+"/inputs",
      element: "#variable-numberOfLikes",
      title: "Setting up inputs",
      content: "This is where you set up the values you pass to Fuzzy.ai. First up is the number of likes."
    },
    {
      path: "/agent/"+agentID+"/inputs",
      element: "#variable-numberOfShares",
      title: "Input values",
      content: "You can set up different category thresholds for each input. In this case, it goes from veryLow to veryHigh."
    },
    {
      path: "/agent/"+agentID+"/inputs",
      element: "#variable-age",
      title: "Input values",
      content: "And in the case of the age input, it’s brandNew to old."
    },
    {
      path: "/agent/"+agentID+"/outputs",
      element: "#variable-relevance",
      title: "Output values",
      content: "Here you can set up the data that will be output. In the case of relevance, it’s a value from 0 to 100, from veryLow to veryHigh."
    },
    {
      path: "/agent/"+agentID+"/rules",
      element: "#rule-list",
      title: "Rules",
      content: "Map input values to output values using rules. Fuzzy.ai is particularly good at evaluating contradictory rules."
    },
    {
      path: "/agent/"+agentID+"/test",
      element: "#test-numberOfLikes",
      title: "Test your agent",
      content: "Test your agent by entering input values and seeing what results are produced. Try it now with 20 likes, 20 shares and an age of 40, then try it with a much higher number of likes and shares and see how it changes!"
    },
    {
      path: "/agent/"+agentID+"/test",
      element: "#new-agent",
      title: "Create a new agent",
      content: "Continue testing this agent, or start creating your own!"
    }
  ];

  var onEnd = function(tour) {
    $.post('/end-tour', function(data) {
        // Should we do something here?
        ;
    });
  };

  // Instance the tour
  var tour;

  tour = new Tour({steps: steps, onEnd: onEnd});

  // Initialize the tour
  tour.init();

  // Start the tour
  tour.start();
};
