// add class to masthead of logged

// Responsive nav


// $(document).ready(function(){
//         $("#nav-mobile").html($(".multi-level-nav").html());
//         $("#nav-trigger span").click(function(){
//             if ($("nav#nav-mobile ul").hasClass("expanded")) {
//                 $("nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
//                 $(this).removeClass("open");
//             } else {
//                 $("nav#nav-mobile ul").addClass("expanded").slideDown(250);
//                 $(this).addClass("open");
//             }
//         });
//     });


$(document).ready(function(){

$('a.burger-lines').click(function(){
  $(this).toggleClass('actived-menu');
  return false;
});

});




var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
	var barChartData = {
		labels : [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
			datasets : [
			{
				fillColor : "rgba(52,55,72,0.5)",
				strokeColor : "rgba(52,55,72,0.8)",
				highlightFill: "rgba(52,55,72,0.75)",
				highlightStroke: "rgba(52,55,72,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			}
		]
	}
	window.onload = function(){
		var ctx = document.getElementById("chart").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true,
			scaleGridLineColor : "rgba(0,0,0,0)",
			scaleLineColor : "rgba(0,0,0,.05)"
		});
	}
