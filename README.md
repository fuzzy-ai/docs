# Fuzzy.ai docs server

This is the half-forked docs server, branched off from `home` to slim down the
size of the initial download. It's not fully forked!

## LICENSE

Code is AGPL-3.0

Documentation itself is CC-BY-SA 4.0 https://creativecommons.org/licenses/by-sa/4.0/

## Environment

TBD